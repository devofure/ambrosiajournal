<!-- 
https://github.com/othneildrew/Best-README-Template/blob/master/README.md
https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

# Ambrosia Journal

<img src="/screenshot/device-2020-08-26-003425.png"  width="280" height="500">

<img src="/screenshot/device-2020-08-26-005034.png"  width="280" height="500">

<img src="/screenshot/device-2020-08-26-003709.png"  width="280" height="500">

<img src="/screenshot/device-2020-08-26-004131.png"  width="280" height="500">

<img src="/screenshot/device-2020-08-26-003955.png"  width="280" height="500">

<img src="/screenshot/device-2020-08-26-003727.png"  width="280" height="500">


<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [Roadmap](#roadmap)
* [Contributing](#contributing)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

<!-- ABOUT THE PROJECT -->
## About The Project
This app is for the food passionated people that want to rate their dishes from any restaurant they visit.

1. Search for a restaurant.
2. Add the dish or item menu you have enjoyed.
3. Rate the dish or the restaurant.
4. add notes to the dish to record the experience.

### Built With
- Google Places Rest API
- Target SDK version 30
- Kotlin 1.4.0
- Android Jetpack libraries


<!-- GETTING STARTED -->
## Getting Started
A single activity app with 3 layers architecture. Data, UI and domain layer but at the moment the domain layer only contains domain models.


- **Outside the layers:** The MainActivity, Application and a utils folder.
- **UI layer:** Contains a utils folder and each major Android Fragment have its own folder with a viewModel.
- **Domain layer:** Contians domain models.
- **Data layer:** Contains the repository that connects to the database and the network client.
- **di:** Contains the AppModule for Jetpack Hilt.

### Prerequisites
- Android Studio 4.2 Canary 7 or newer
- Android SDK 30

### Installation
1. Get the places Api key: https://developers.google.com/places/android-sdk/get-api-key
2. Clone the project
3. Create an Android string resource file with the name "*sensitive_strings.xml*"
4. Add the folllowing strings:
```xml
  <resources>
    <string name="google_places_api_key">{your own API key}</string>
    <string name="developer_email">example@gmail.com</string>
    <string name="project_repository">https://gitlab.com/devofure/meal-mania</string>
    <string name="linkedin">https://www.linkedin.com/</string>
  </resources>
```

<!-- USAGE EXAMPLES -->
## Usage

<!-- ROADMAP -->
## Roadmap

<!-- CONTRIBUTING -->
## Contributing

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/featureName`)
3. Commit your Changes (`git commit -m 'Add a feature'`)
4. Push to the Branch (`git push origin feature/featureName`)
5. Open a Pull Request



<!-- LICENSE -->
## License

<!-- CONTACT -->
## Contact

<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=flat-square&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/erick-chavez-alcarraz-75936583