package com.devofure.ambrosiajournal.domain

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test

class GpsUtilsKtTest {

    @Test
    fun createLocationGridTestOutOfDistance() {
        //500 kilometers of distance
        val openLocationCode1 = createLocationGridOutOfDistance(
            59.2604305053163,
            18.03448055356654,
        )
        val openLocationCode2 = createLocationGridOutOfDistance(
            59.26074306903159,
            18.025711334039293,
        )

        assertNotEquals(
            openLocationCode1.key,
            openLocationCode2.key
        )
    }

    @Test
    fun createLocationGridTestWithinDistance() {
        //30 meters of distance
        val openLocationCode1 = createLocationGridOutOfDistance(
            59.2628415,
            18.0155494,
        )
        val openLocationCode2 = createLocationGridOutOfDistance(
            59.26304468256846,
            18.015906195468578,
        )

        assertEquals(
            openLocationCode1.key,
            openLocationCode2.key
        )
    }
}