package com.devofure.ambrosiajournal.domain

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import com.google.openlocationcode.OpenLocationCode

private const val CODE_LENGTH_TO_GENERATE = 10

/**
 * Higher means greater grid area cover
 */
const val GRID_SIZE_LEVEL = 3

fun createLocationGridOutOfDistance(latitude: Double, longitude: Double): LocationGrid {
    val openLocationCode = OpenLocationCode(latitude, longitude, CODE_LENGTH_TO_GENERATE)
    return LocationGrid(
        openLocationCode.code.dropLast(GRID_SIZE_LEVEL),
        latitude,
        longitude,
        openLocationCode,
    )
}

suspend fun generateDistance(
    place: Restaurant,
    startLocation: LatLng,
): Double {
    val endLocation = LatLng(place.location.lat, place.location.lng)
    return SphericalUtil.computeDistanceBetween(startLocation, endLocation)
}


data class LocationGrid(
    /**
     * the key is unique within a grid area
     */
    val key: String,
    val currentLatitude: Double,
    val currentLongitude: Double,
    val openLocationCode: OpenLocationCode,
)