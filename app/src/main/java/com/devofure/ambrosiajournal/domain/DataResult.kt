package com.devofure.ambrosiajournal.domain

sealed class DataResult<out R> {
    object Loading : DataResult<Nothing>()
    data class Success<out R>(val data: R, val isCache: Boolean) : DataResult<R>()
    data class Error<out R>(val exception: Throwable, val cache: R? = null) : DataResult<R>()
}