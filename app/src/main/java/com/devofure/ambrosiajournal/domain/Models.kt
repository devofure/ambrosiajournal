package com.devofure.ambrosiajournal.domain

import androidx.annotation.Keep
import com.devofure.ambrosiajournal.presenter.adapter.DomainModel
import com.google.android.gms.maps.model.LatLng

@Keep
data class MenuItem(
    override val id: String? = null,
    var name: String,
    val restaurantId: String,
    var favorite: Boolean? = null,
    var userRating: Int? = null,
    val info: String? = null,
    val price: String? = null,
    val imageUrl: String? = null,
    val menuItemOriginalName: String? = null,
    val tags: List<String>? = null,
    val restaurant: Restaurant? = null,
) : DomainModel

@Keep
data class Restaurant(
    val info: String? = null,
    var favorite: Boolean? = null,
    val mainPhoto: String? = null,
    val tags: List<String>? = emptyList(),
    val businessStatus: String?,
    val formattedAddress: String?,
    val formattedPhoneNumber: String?,
    val location: Location,
    val icon: String?,
    val internationalPhoneNumber: String?,
    val name: String,
    val openHours: OpenHours?,
    val photos: List<Photo>? = emptyList(),
    override val id: String,
    val priceLevel: Int?,
    val googleRating: Double?,
    var userRating: Int?,
    val reference: String?,
    val reviews: List<GoogleReview>? = emptyList(),
    val scope: String?,
    val types: List<String> = emptyList(),
    val url: String?,
    val userRatingsTotal: Double?,
    val vicinity: String?,
    val website: String?,
    val lastChanged: Long,
    //if the data is from the detail and could be more
    val isCompleteData: Boolean?,
    var currentDistance: Double? = null,//distance from the user
) : DomainModel {

    override fun <C : DomainModel> isSameContent(other: C): Boolean {
        if (other is Restaurant) {
            return other.currentDistance == this.currentDistance
                    && other.name == this.name
                    && other.favorite == this.favorite
                    && other.userRating == this.userRating
                    && other.formattedAddress == this.formattedAddress
                    && other.vicinity == this.vicinity
        }
        return super.isSameContent(other)
    }
}

suspend fun Restaurant.generateDistance(startLocation: LatLng) {
    this.currentDistance = generateDistance(this, startLocation)
}

data class GoogleReview(
    val authorName: String,
    val authorUrl: String,
    val language: String,
    val profilePhotoUrl: String,
    val rating: Int,
    val relativeTimeDescription: String,
    val text: String,
    val time: Int,
)

data class UserReviewNote(
    val relativeTimeDescription: String? = null,
    var text: String = "",
    val lastChanged: Long,
    override val id: String,
    val menuItemId: String,
) : DomainModel

data class Photo(
    override val id: String? = null,
    val height: Long,
    val width: Long,
    val parentId: String,
    val photoReference: String,
    val htmlAttributes: List<HtmlAttribute> = emptyList(),
    val localPah: String?,
) : DomainModel

data class SearchQuery(
    val textQuery: String? = null,
    val distanceInMeters: Long? = null,
    val locationGrid: LocationGrid? = null,
    val types: List<String>,
)

data class HtmlAttribute(
    val name: String,
)

data class Location(
    val lat: Double, val lng: Double,
)

data class OpenHours(
    val openNow: Boolean?,
    val periods: List<DayPeriod>?,
    val weekday_text: List<String>?,
)

data class DayPeriod(val open: DayPeriodLimit, val close: DayPeriodLimit?) {
    override fun toString(): String = open.time + " - " + close?.time
}

data class DayPeriodLimit(val day: String, val time: String)

fun List<String>.mapToCommaSeparated(): String = this.joinToString(separator = ",")