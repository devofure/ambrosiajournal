package com.devofure.ambrosiajournal.di

import android.content.Context
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.data.local.AppDatabase
import com.devofure.ambrosiajournal.data.network.PlacesRestClientService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    @Named("GooglePlacesApi")
    fun providePlacesApi(@ApplicationContext appContext: Context) =
        appContext.getString(R.string.google_places_api_key)

    @Singleton
    @Provides
    fun provideRestaurantDao(db: AppDatabase) = db.placeDao()

    @Singleton
    @Provides
    fun provideMenuItemDao(db: AppDatabase) = db.menuItemDao()

    @Singleton
    @Provides
    fun provideRestaurantPhotoDao(db: AppDatabase) = db.placePhotoDao()

    @Singleton
    @Provides
    fun provideMenuItemPhotoDao(db: AppDatabase) = db.menuItemPhotoDao()

    @Singleton
    @Provides
    fun provideReviewDao(db: AppDatabase) = db.reviewDao()

    @Singleton
    @Provides
    fun provideReviewNoteDao(db: AppDatabase) = db.reviewNoteDao()

    @Singleton
    @Provides
    fun provideDayPeriodDao(db: AppDatabase) = db.dayPeriodDao()

    @Singleton
    @Provides
    fun provideSearchCacheDao(db: AppDatabase) = db.searchCacheDao()

    @Singleton
    @Provides
    fun provideSearchRestaurantCacheDao(db: AppDatabase) = db.searchPlaceCacheDao()

    @Singleton
    @Provides
    fun providePlacesRestClientService(): PlacesRestClientService {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client: OkHttpClient =
            OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
            .baseUrl("https://maps.googleapis.com/maps/api/place/")
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(PlacesRestClientService::class.java)
    }
}