package com.devofure.ambrosiajournal.hardware

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Looper
import com.devofure.ambrosiajournal.utils.await
import com.google.android.gms.location.*
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

class LocationRetriever(private val context: Context) {

    companion object {
        const val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 5000
    }

    private val fusedLocationClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(context.applicationContext)
    }
    private val locationRequest: LocationRequest by lazy {
        LocationRequest().apply {
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            numUpdates = 1
            expirationTime = UPDATE_INTERVAL_IN_MILLISECONDS * 2
        }
    }

    @SuppressLint("MissingPermission")
    suspend fun getLocation(): Location? {
        val lastLocation = fusedLocationClient.lastLocation?.await()
        // Got last known location. In some rare situations this can be null.
        return if (lastLocation != null && lessThanTwoMinutesOld(lastLocation)) {
            lastLocation
        } else {
            getLocationUpdateOnce()
        }
    }

    private fun lessThanTwoMinutesOld(lastLocation: Location) =
        lastLocation.time >= System.currentTimeMillis() - (1000 * 120)

    @SuppressLint("MissingPermission")
    private suspend fun getLocationUpdateOnce(): Location? =
        suspendCancellableCoroutine { continuation ->

            val locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    fusedLocationClient.removeLocationUpdates(this)
                    if (locationResult?.lastLocation != null) {
                        continuation.resume(locationResult.lastLocation)
                    } else {
                        continuation.cancel()
                    }
                }
            }

            //the locationRequest will make it  only fetch once
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper(),
            )
        }
}

