package com.devofure.ambrosiajournal.data.local

import androidx.annotation.Keep
import androidx.annotation.NonNull
import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import java.util.*

@Keep
data class PlaceCompleteEntity(
    @Embedded val placeEntity: PlaceEntity,
    @Relation(
        parentColumn = "place_id",
        entityColumn = "place_id",
        entity = DayPeriodEntity::class,
    ) val openHours: List<DayPeriodEntity>?,
)

@Keep
data class MenuItemCompleteEntity(
    @Embedded val menuItemEntity: MenuItemEntity,
    @Relation(
        parentColumn = "place_id",
        entityColumn = "place_id"
    ) val placeEntity: PlaceEntity,
)

@Keep
@Entity(tableName = "place")
data class PlaceEntity(
    var info: String? = null,
    //user data
    var favorite: Boolean? = null,
    val mainPhoto: String? = null,
    val business_status: String?,
    val formatted_address: String?,
    val formatted_phone_number: String?,
    @Embedded val location: LocationEntity,
    val icon: String?,
    val international_phone_number: String?,
    val name: String,
    @PrimaryKey
    val place_id: String,
    val price_level: Int?,
    val google_rating: Double?,
    val reference: String?,
    val scope: String?,
    val user_ratings_total: Double?,
    val vicinity: String?,
    //user data
    var user_rating: Int?,
    val url: String?,
    val website: String?,
    val last_changed: Long,
    val is_complete_data: Boolean?,
)

@Keep
@Entity(
    tableName = "menu_item",
    foreignKeys = [ForeignKey(
        entity = PlaceEntity::class,
        parentColumns = arrayOf("place_id"),
        childColumns = arrayOf("place_id"),
        onDelete = CASCADE
    )],
    indices = [Index("place_id")]
)
data class MenuItemEntity(
    @PrimaryKey
    val id: String = UUID.randomUUID().toString(),
    val place_id: String,
    var favorite: Boolean? = null,
    val name: String,
    val user_rating: Int? = null,
    val info: String? = null,
    val category: String? = null,
    val price: String? = null,
    val menu_item_original_name: String? = null,
    val image_url: String? = null,
)

@Keep
data class SearchCacheCompleteEntity(
    @Embedded val searchCacheEntity: SearchCacheEntity?,
    @Relation(
        parentColumn = "id",//SearchCacheCompleteEntity
        entityColumn = "place_id",
        associateBy = Junction(SearchPlaceCacheEntity::class),
        entity = PlaceEntity::class
    ) val places: List<PlaceEntity>?,
)

@Keep
@Entity(
    primaryKeys = ["id", "place_id"],
    tableName = "search_cache__place",
    indices = [Index("place_id")],
)
data class SearchPlaceCacheEntity(
    val id: String,
    val place_id: String,
)

@Keep
@Entity(
    tableName = "search_cache",
    indices = [Index("text_query", "distance_in_meters", "position_code", "types", unique = true)],
)
data class SearchCacheEntity(
    @PrimaryKey
    val id: String,
    val text_query: String = "",
    val distance_in_meters: Long = 0,
    val position_code: String = "",
    val types: String,
    val created_millis: Long,
) {
    companion object
}

@Keep
@Entity(tableName = "google_review")
data class GoogleReviewEntity(
    @PrimaryKey
    val id: String,
    val place_id: String,
    val author_name: String,
    val author_url: String,
    val language: String,
    val profile_photo_url: String,
    val rating: Int,
    //The relative time that the review was submitted.
    val relative_time_description: String,
    val text: String,
    val created_millis: Int,
)

@Keep
@Entity(
    tableName = "user_review_note",
    foreignKeys = [ForeignKey(
        entity = MenuItemEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("menu_item_id"),
        onDelete = CASCADE
    )],
    indices = [Index("menu_item_id")],
)
data class UserReviewNoteEntity(
    @PrimaryKey @NonNull
    val id: String,
    @ColumnInfo(name = "menu_item_id")
    val menu_item_id: String,
    val relative_time_description: String?,
    val text: String,
    val last_change: Long,
)

@Keep
@Entity(tableName = "place_type")
data class PlaceTypesEntity(
    @PrimaryKey @NonNull
    val id: String,
    val place_id: String,
    val type: String,
)

@Keep
@Entity(
    tableName = "photos",
    foreignKeys = [ForeignKey(
        entity = PlaceEntity::class,
        parentColumns = arrayOf("place_id"),
        childColumns = arrayOf("place_id")
    )],
    indices = [Index("place_id")],
)
data class PlacePhotoEntity(
    @PrimaryKey @ColumnInfo(name = "id")
    val photo_reference: String,
    val place_id: String,
    val height: Long,
    val width: Long,
)

@Keep
@Entity(
    tableName = "menu_item_photos",
    foreignKeys = [ForeignKey(
        entity = MenuItemEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("menu_item_id")
    )],
    indices = [Index("menu_item_id")],
)
data class MenuItemPhotoEntity(
    @PrimaryKey @ColumnInfo(name = "id")
    val photo_reference: String,
    val description: String?,
    val local_pah: String?,
    val menu_item_id: String,
    val height: Long,
    val width: Long,
)

@Keep
data class LocationEntity(
    val lat: Double, val lng: Double,
)

@Keep
@Entity(
    primaryKeys = ["place_id", "open_day"],
    tableName = "day_period",
    foreignKeys = [ForeignKey(
        entity = PlaceEntity::class,
        parentColumns = arrayOf("place_id"),
        childColumns = arrayOf("place_id")
    )],
    indices = [Index("place_id")],
)
data class DayPeriodEntity(
    val place_id: String,
    val open_day: String,
    val open_time: String,
    val close_day: String?,
    val close_time: String?,
)

@Keep
data class UpdatePlaceRateEntity(
    val place_id: String,
    val user_rating: Int?,
)

@Keep
data class UpdateMenuItemRateEntity(
    val id: String,
    val user_rating: Int?,
)

@Keep
data class UpdateMenuItemNameEntity(
    val id: String,
    val name: String,
)

@Keep
data class UpdatePlaceFavoriteEntity(
    val place_id: String,
    val favorite: Boolean,
)

@Keep
data class UpdateMenuItemFavoriteEntity(
    val id: String,
    val favorite: Boolean,
)

@Keep
data class UpdateMenuItemMainImageEntity(
    val id: String,
    val image_url: String,
)