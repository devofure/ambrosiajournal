package com.devofure.ambrosiajournal.data

class NoDataAvailable(message: String) : Throwable(message)
class NoInternetAvailable(message: String) : Throwable(message)
class UnknownProblem(message: String) : Throwable(message)
