package com.devofure.ambrosiajournal.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [
        PlaceEntity::class,
        MenuItemEntity::class,
        PlacePhotoEntity::class,
        MenuItemPhotoEntity::class,
        GoogleReviewEntity::class,
        PlaceTypesEntity::class,
        UserReviewNoteEntity::class,
        DayPeriodEntity::class,
        SearchCacheEntity::class,
        SearchPlaceCacheEntity::class,
    ],
    version = 2,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun placeDao(): PlaceDao
    abstract fun menuItemDao(): MenuItemDao
    abstract fun placePhotoDao(): PlacePhotoDao
    abstract fun menuItemPhotoDao(): MenuItemPhotoDao
    abstract fun reviewDao(): GoogleReviewDao
    abstract fun reviewNoteDao(): UserReviewNoteDao
    abstract fun dayPeriodDao(): DayPeriodDao
    abstract fun searchCacheDao(): SearchCacheDao
    abstract fun searchPlaceCacheDao(): SearchPlaceCacheDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            instance
                ?: synchronized(this) {
                    instance
                        ?: buildDatabase(
                            context
                        )
                            .also { instance = it }
                }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabase::class.java, "ambrosia_journal")
                .fallbackToDestructiveMigration()
                .build()
    }
}