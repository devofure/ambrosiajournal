package com.devofure.ambrosiajournal.data

import androidx.room.withTransaction
import com.devofure.ambrosiajournal.data.local.*
import com.devofure.ambrosiajournal.data.network.*
import com.devofure.ambrosiajournal.domain.*
import com.devofure.ambrosiajournal.domain.DataResult.Error
import com.devofure.ambrosiajournal.presenter.base.Event
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import java.lang.System.currentTimeMillis
import java.net.UnknownHostException
import javax.inject.Inject
import javax.inject.Named

const val OLD_DATA_THRESHOLD: Long = 5184000000 // 60 days in millis
const val TYPE_RESTAURANT: String = "restaurant"
@Suppress("unused")
const val TYPE_FOOD: String = "food"
const val TYPE_CAFE: String = "cafe"
const val TYPE_DELIVERY: String = "meal_delivery"
const val REST_RESULT_OK: String = "OK"

@ExperimentalCoroutinesApi
class AmbrosiaJournalRepository @Inject constructor(
    private val database: AppDatabase,
    @Named("GooglePlacesApi") private val placesApiKey: String,
    private val placesRestClient: PlacesRestClientService,
    private val restaurantDao: PlaceDao,
    private val photoDao: PlacePhotoDao,
    private val menuItemDao: MenuItemDao,
    private val menuItemPhotoDao: MenuItemPhotoDao,
    private val reviewNoteDao: UserReviewNoteDao,
    private val dayPeriodDao: DayPeriodDao,
    private val searchCacheDao: SearchCacheDao,
    private val searchRestaurantCacheDao: SearchPlaceCacheDao,
) {

    fun retrieveRestaurantByName(
        textQuery: String,
        types: List<String>,
    ): Flow<Event<DataResult<List<Restaurant>>>> =
        flow {
            emit(DataResult.Loading)
            val searchQuery = SearchQuery(types = types, textQuery = textQuery)
            //Load cache
            val cacheEntity = searchCacheDao.loadSearchQueryCache(
                searchQuery.textQuery!!,
                searchQuery.types.mapToCommaSeparated(),
            )

            if (cacheEntity != null)
                emit(DataResult.Success(cacheEntity.places!!.map {
                    it.mapToDomain()
                }, true))
            //Check best before date then fetch and save if necessary
            if (shouldFetchAgain(cacheEntity)) {
                val freshData: DataResult<List<Restaurant>> = handleFetchedData {
                    placesRestClient.findPlaceFromText(
                        textQuery,
                        InputType.TextQuery().value,
                        types.mapToCommaSeparated(),
                        placesApiKey
                    )
                }
                if (freshData is DataResult.Success) {
                    saveSearchData(freshData.data, searchQuery)
                }
                //Response the rest response, success or failed
                emit(freshData)
            }
        }.map { Event(it) }

    suspend fun syncPlace(placeId: String, force: Boolean = false) {
        val cacheEntity = restaurantDao.getPlaceCompleteByIdOnce(placeId)
        if (force || shouldFetchAgain(cacheEntity?.placeEntity)) {
            val placeNetworkResponse =
                placesRestClient.fetchPlaceDetails(placeId, placeFields(), placesApiKey)
            if (placeNetworkResponse?.status == REST_RESULT_OK) {
                val newPlace =
                    placeNetworkResponse.result.mapToDomain(currentTimeMillis(), true)
                database.withTransaction {
                    val completeRestaurant = newPlace.mapToCompleteEntity()
                    restaurantDao.upsertPlaceCompleteEntity(completeRestaurant)
                    val photos = newPlace.photos?.map { it.mapToEntity(newPlace.id) }
                    if (photos != null) {
                        photoDao.replace(*photos.toTypedArray())
                    }
                    val openHours = completeRestaurant.openHours
                    if (openHours != null) {
                        dayPeriodDao.replace(*openHours.toTypedArray())
                    }
                }
            } else throw NoDataAvailable("Could not retrieve data")
        }
    }

    fun retrievePlace(restaurantId: String): Flow<DataResult<Restaurant>> = flow {
        val result = restaurantDao
            .getPlaceCompleteByIdDistinct(restaurantId)
            .onStart { emit(DataResult.Loading) }
            .map { restaurantCompleteEntityCache ->
                restaurantCompleteEntityCache?.mapToDomain()?.let {
                    DataResult.Success(it, true)
                } ?: Error(NoDataAvailable(restaurantId))
            }
        emitAll(result)
    }

    private fun shouldFetchAgain(cacheEntity: PlaceEntity?): Boolean =
        cacheEntity == null || cacheEntity.is_complete_data == false || dataIsOld(cacheEntity.last_changed)

    private fun dataIsOld(cacheTime: Long): Boolean =
        cacheTime + OLD_DATA_THRESHOLD < currentTimeMillis()

    fun retrieveRestaurantMenu(restaurantId: String): Flow<DataResult<List<MenuItem>>> = flow {
        val result = menuItemDao
            .loadDistinctPlaceMenuById(restaurantId)
            .onStart { emit(DataResult.Loading) }
            .map { list -> list?.map { it.menuItemEntity.mapToDomain(it.placeEntity) } }
            .map {
                if (it != null) DataResult.Success(it, true)
                else Error(NoDataAvailable(restaurantId))
            }
        emitAll(result)
    }

    fun retrieveMenuItemPhotos(menuItemId: String): Flow<DataResult<List<Photo>>> = flow {
        val result = menuItemPhotoDao
            .loadDistinctMenuItemPhotos(menuItemId)
            .onStart { emit(DataResult.Loading) }
            .map { list -> list?.map { it.mapToDomain() } }
            .map {
                if (it != null) DataResult.Success(it, true)
                else Error(NoDataAvailable(menuItemId))
            }
        emitAll(result)
    }

    fun retrieveRestaurantPhotos(restaurantId: String): Flow<DataResult<List<Photo>>> = flow {
        val result = photoDao
            .loadDistinctPlacePhotos(restaurantId)
            .onStart { emit(DataResult.Loading) }
            .map { list -> list?.map { it.mapToDomain() } }
            .map {
                if (it != null) DataResult.Success(it, true)
                else Error(NoDataAvailable(restaurantId))
            }
        emitAll(result)
    }

    fun retrieveUserReviewNoteList(menuItemId: String): Flow<DataResult<List<UserReviewNote>>> =
        flow {
            val result = reviewNoteDao
                .loadDistinctReviewNotesByMenuItemId(menuItemId)
                .onStart { emit(DataResult.Loading) }
                .map { list -> list?.map { it.mapToDomain() } }
                .map {
                    if (it != null) DataResult.Success(it, true)
                    else Error(NoDataAvailable(menuItemId))
                }
            emitAll(result)
        }

    fun retrieveNearbyPlaces(
        location: LocationGrid,
        radiusInMeters: Long,
        vararg type: String,
    ): Flow<Event<DataResult<List<Restaurant>>>> {
        val startLocation = LatLng(location.currentLatitude, location.currentLongitude)
        return retrieveNearbyPlacesFlow(location, radiusInMeters, *type)
            .distinctUntilChanged()
            .mapLatest { dataResult ->
                if (dataResult is DataResult.Success) {
                    val sortedPlaces = dataResult.data
                        .onEach { it.generateDistance(startLocation) }
                        .sortedBy { it.currentDistance }
                    Event(DataResult.Success(sortedPlaces, dataResult.isCache))
                } else Event(dataResult)
            }
    }

    private fun retrieveNearbyPlacesFlow(
        location: LocationGrid,
        radiusInMeters: Long,
        vararg type: String,
    ): Flow<DataResult<List<Restaurant>>> = flow {
        emit(DataResult.Loading)

        val searchQuery = SearchQuery(
            types = type.asList(),
            locationGrid = location,
            distanceInMeters = radiusInMeters
        )
        //Load cache
        val cacheEntity = loadSearchNearbyCacheSafe(searchQuery)
        //Check best before date then fetch and save if necessary
        if (shouldFetchAgain(cacheEntity)) {
            val freshData: DataResult<List<Restaurant>> =
                handleFetchedData { fetchNearPlaces(searchQuery) }
            if (freshData is DataResult.Success) {
                saveSearchData(freshData.data, searchQuery)
                val cachePlaces = loadSearchNearbyCacheSafe(searchQuery)?.places!!
                    .map { it.mapToDomain() }
                emit(DataResult.Success(cachePlaces, false))
            } else {
                // was not success
                emit(freshData)
            }
        } else {
            //will never be null
            val cacheDomainModel = cacheEntity!!.places?.map { it.mapToDomain() } ?: emptyList()
            emit(DataResult.Success(cacheDomainModel, true))
        }
    }

    private suspend fun loadSearchNearbyCacheSafe(searchQuery: SearchQuery): SearchCacheCompleteEntity? {
        return try {
            searchCacheDao.loadSearchNearbyCache(
                searchQuery.distanceInMeters!!,
                searchQuery.types.mapToCommaSeparated(),
                searchQuery.locationGrid!!.key
            )
        } catch (exception: Throwable) {
            FirebaseCrashlytics.getInstance().log(searchQuery.toString())
            FirebaseCrashlytics.getInstance().recordException(exception)
            exception.printStackTrace()
            null
        }
    }

    private fun shouldFetchAgain(cacheEntity: SearchCacheCompleteEntity?): Boolean =
        cacheEntity == null || dataIsOld(cacheEntity.searchCacheEntity!!.created_millis)

    private suspend fun handleFetchedData(searchPlaces: suspend () -> PlaceListResponse?) =
        try {
            val freshData: PlaceListResponse? = searchPlaces.invoke()
            if (freshData != null && freshData.status == REST_RESULT_OK)
                DataResult.Success(
                    freshData.results.map { it.mapToDomain(currentTimeMillis(), false) },
                    false
                )
            else Error(NoDataAvailable(""))
        } catch (e: Throwable) {
            FirebaseCrashlytics.getInstance().recordException(e)
            notifyDataError(e)
        }

    private fun <I> notifyDataError(e: Throwable, cacheData: I? = null): Error<I> =
        when (e) {
            is UnknownHostException -> Error(NoInternetAvailable(e.message ?: ""), cacheData)
            else -> Error(UnknownProblem(e.message ?: ""), cacheData)
        }

    private suspend fun fetchNearPlaces(searchQuery: SearchQuery): PlaceListResponse? =
        placesRestClient
            .fetchPlacesNearby(
                locationRest(
                    searchQuery.locationGrid!!.currentLatitude,
                    searchQuery.locationGrid.currentLongitude
                ),
                searchQuery.types.mapToCommaSeparated(),
                searchQuery.distanceInMeters!!,
                placesApiKey
            )

    private suspend fun saveSearchData(restaurantList: List<Restaurant>, searchQuery: SearchQuery) =
        database.withTransaction {
            val restaurantEntity: List<PlaceEntity> =
                restaurantList.map { it.mapToEntity(false) }
            val typesSeparatedByComma = searchQuery.types.mapToCommaSeparated()
            val currentTime = currentTimeMillis()
            val searchCacheEntity = SearchCacheEntity.createEntity(
                text_query = searchQuery.textQuery ?: "",
                distance_in_meters = searchQuery.distanceInMeters ?: 0,
                position_code = searchQuery.locationGrid?.key ?: "",
                types = typesSeparatedByComma,
                created_millis = currentTime,
            )
            restaurantDao.insertElseIgnore(*restaurantEntity.toTypedArray())
            searchCacheDao.insertAndReplace(searchCacheEntity)
            searchRestaurantCacheDao.insertAndReplace(
                *restaurantEntity.map {
                    SearchPlaceCacheEntity(searchCacheEntity.id, it.place_id)
                }.toTypedArray()
            )
        }

    private fun locationRest(currentLatitude: Double, currentLongitude: Double): String =
        "$currentLatitude,$currentLongitude"

    fun loadBookmarkRestaurants(): Flow<DataResult<List<Restaurant>>> = flow {
        emit(DataResult.Loading)
        val result = restaurantDao.getBookmarkPlaceDistinct()
            .map { list -> list.map { it.mapToDomain() } }
            //.mapLatest { dataResult ->}
            .map { DataResult.Success(it, true) }
        emitAll(result)
    }

    fun loadBookmarkedMenuItems(): Flow<DataResult<List<MenuItem>>> = flow {
        emit(DataResult.Loading)
        val result = menuItemDao.getBookmarkMenuItemDistinct()
            .map { list -> list.map { it.menuItemEntity.mapToDomain(it.placeEntity) } }
            .map { DataResult.Success(it, true) }
        emitAll(result)
    }

    suspend fun saveNewMenuItem(name: String, placeId: String) =
        menuItemDao.insertElseIgnore(MenuItemEntity(name = name, place_id = placeId))

    suspend fun updateMenuItemName(itemMenuId: String, name: String) =
        menuItemDao.updateName(UpdateMenuItemNameEntity(itemMenuId, name = name))

    suspend fun saveOrReplace(item: UserReviewNote) =
        reviewNoteDao.insertAndReplace(item.mapToEntity())

    fun retrieveMenuItem(menuItemId: String): Flow<DataResult<MenuItem>> = flow {
        val result = menuItemDao
            .getMenuItemCompleteByIdDistinct(menuItemId)
            .onStart { emit(DataResult.Loading) }
            .map { it?.menuItemEntity?.mapToDomain(it.placeEntity) }
            .map {
                if (it != null) DataResult.Success(it, true)
                else Error(NoDataAvailable(menuItemId))
            }
        emitAll(result)
    }

    fun retrieveUserReviewNote(reviewNoteId: String): Flow<DataResult<UserReviewNote>> = flow {
        val result = reviewNoteDao
            .loadDistinctReviewNoteByMenuItemId(reviewNoteId)
            .onStart { emit(DataResult.Loading) }
            .map { it?.mapToDomain() }
            .map {
                if (it != null) DataResult.Success(it, true)
                else Error(NoDataAvailable(reviewNoteId))
            }
        emitAll(result)
    }

    suspend fun deleteMenuItem(menuItem: MenuItem) {
        menuItemDao.delete(menuItem.mapToEntity())
    }

    suspend fun deleteReviewNote(userReviewNote: UserReviewNote) {
        reviewNoteDao.delete(userReviewNote.mapToEntity())
    }

    suspend fun updateRestaurantRate(restaurantId: String, rate: Int) {
        restaurantDao.updateRate(UpdatePlaceRateEntity(restaurantId, rate))
    }

    suspend fun updateMenuItemRate(menuItemId: String, rate: Int) {
        menuItemDao.updateRate(UpdateMenuItemRateEntity(menuItemId, rate))
    }

    suspend fun updateMenuItemFavorite(menuItemId: String, newFavoriteValue: Boolean) {
        menuItemDao.updateFavorite(UpdateMenuItemFavoriteEntity(menuItemId, newFavoriteValue))
    }

    suspend fun updateRestaurantFavorite(restaurantId: String, newFavoriteValue: Boolean) {
        restaurantDao.updateFavorite(UpdatePlaceFavoriteEntity(restaurantId, newFavoriteValue))
    }

    suspend fun addImageToMenuItem(
        menuItemId: String,
        description: String?,
        path: String,
        name: String,
        maxHeight: Long,
        maxWidth: Long,
    ) {
        menuItemPhotoDao.insertElseIgnore(MenuItemPhotoEntity(
            name,
            description,
            path,
            menuItemId,
            maxHeight,
            maxWidth,
        ))
    }

    suspend fun insertOrIgnorePhoto(menuItemId: String, absolutePath: String) {
        menuItemPhotoDao.insertOrIgnoreMainPhoto(UpdateMenuItemMainImageEntity(menuItemId,
            absolutePath))
    }
}