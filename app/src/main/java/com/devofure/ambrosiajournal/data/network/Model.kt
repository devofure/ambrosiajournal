package com.devofure.ambrosiajournal.data.network

import androidx.annotation.Keep

@Keep
data class PlaceListResponse(
    val html_attributes: List<HtmlAttributeNM>,
    val results: List<PlaceNM>,
    val status: String,
)

@Keep
data class PlaceResponse(
    val html_attributes: List<HtmlAttributeNM>,
    val result: PlaceNM,
    val status: String,
)

@Keep
data class PlaceNM(
    val address_component: List<AddressComponentNM> = emptyList(),
    val adr_address: String,
    val business_status: String? = null,
    val formatted_address: String? = null,
    val formatted_phone_number: String? = null,
    val geometry: GeometryNM,
    val icon: String? = null,
    val international_phone_number: String? = null,
    val name: String,
    val opening_hours: OpenHoursNM? = null,
    val photos: List<PhotoNM>? = emptyList(),
    val place_id: String,
    val price_level: Int? = null,
    val rating: Double? = null,
    val reference: String? = null,
    val reviews: List<ReviewNM>?,
    val scope: String? = null,
    val types: List<String> = emptyList(),
    val user_ratings_total: Double? = null,
    val url: String?,
    val utc_offset: Int? = null,
    val vicinity: String? = null,
    val website: String? = null,
)

fun placeFields(): String {
    //Basic Fields
    return "address_component," +
            "adr_address," +
            "business_status," +
            "formatted_address," +
            "geometry," +
            "icon," +
            "name," +
            "photos," +
            "place_id," +
            "plus_code," +
            "type," +
            "url," +
            "utc_offset," +
            "vicinity"
    //Contact Fields
    //"formatted_phone_number," +
    //"international_phone_number," +
    //"opening_hours," +
    //"website"
    // Atmosphere
    //price_level, rating, review, user_ratings_total
}

@Keep
data class ReviewNM(
    val author_name: String,
    val author_url: String,
    val language: String,
    val profile_photo_url: String,
    val rating: Int,
    val relative_time_description: String,
    val text: String,
    val time: Int,
)

@Keep
data class AddressComponentNM(
    val long_name: String,
    val short_name: String,
    val types: List<String>,
)

@Keep
data class PhotoNM(
    val height: Long,
    val html_attributes: List<HtmlAttributeNM>?,
    val photo_reference: String,
    val width: Long,
)

@Keep
data class GeometryNM(
    val location: LocationNM,
)

@Keep
data class LocationNM(
    val lat: Double, val lng: Double,
)

@Keep
data class OpenHoursNM(
    val open_now: Boolean,
    val periods: List<DayPeriodNM>,
    val weekday_text: List<String>,
)

@Keep
data class DayPeriodNM(val open: DayPeriodLimitNM?, val close: DayPeriodLimitNM?)

@Keep
data class DayPeriodLimitNM(val day: String, val time: String)

@Keep
data class HtmlAttributeNM(
    val name: String,
)
