package com.devofure.ambrosiajournal.data.local

import com.devofure.ambrosiajournal.domain.*

fun PlaceCompleteEntity.mapToDomain() =
    this.placeEntity.mapToDomain(this.openHours)

fun PlaceEntity.mapToDomain(
    newDayPeriod: List<DayPeriodEntity>? = null,
) = Restaurant(
    lastChanged = this.last_changed,
    favorite = this.favorite ?: false,
    mainPhoto = this.mainPhoto,
    businessStatus = this.business_status,
    formattedAddress = this.formatted_address,
    formattedPhoneNumber = this.formatted_phone_number,
    location = Location(this.location.lat, this.location.lng),
    icon = this.icon,
    openHours = OpenHours(
        openNow = null,
        periods = newDayPeriod?.map { it.mapToDomain() },
        weekday_text = null
    ),
    internationalPhoneNumber = this.international_phone_number,
    name = this.name,
    id = this.place_id,
    priceLevel = this.price_level,
    googleRating = this.google_rating,
    reference = this.reference,
    scope = this.scope,
    userRatingsTotal = this.user_ratings_total,
    url = this.url,
    vicinity = this.vicinity,
    userRating = this.user_rating,
    website = this.website,
    isCompleteData = this.is_complete_data,
)

fun PlacePhotoEntity.mapToDomain() = Photo(
    id = this.photo_reference,
    height = this.height,
    width = this.width,
    parentId = this.place_id,
    photoReference = this.photo_reference,
    localPah = null
)

fun MenuItemPhotoEntity.mapToDomain() = Photo(
    id = this.photo_reference,
    height = this.height,
    width = this.width,
    parentId = this.menu_item_id,
    photoReference = this.photo_reference,
    localPah = this.local_pah
)

fun PlaceEntity.mergeUserData(copyFrom: PlaceEntity) {
    this.favorite = copyFrom.favorite
    this.user_rating = copyFrom.user_rating
}

fun DayPeriodEntity.mapToDomain() = DayPeriod(
    open = DayPeriodLimit(open_day, open_time),
    close = if (this.close_day != null && this.close_time != null) DayPeriodLimit(
        close_day,
        close_time
    ) else null,
)

fun MenuItemEntity.mapToDomain(
    restaurantDomain: PlaceEntity? = null,
) = MenuItem(
    id = this.id,
    name = this.name,
    restaurantId = this.place_id,
    favorite = this.favorite,
    tags = emptyList(),
    info = this.info,
    userRating = this.user_rating,
    price = this.price,
    imageUrl = this.image_url,
    restaurant = restaurantDomain?.mapToDomain()
)

fun MenuItem.mapToEntity() = MenuItemEntity(
    place_id = this.restaurantId,
    info = this.info,
    image_url = this.imageUrl,
    favorite = this.favorite,
    user_rating = this.userRating,
    price = this.price,
    name = this.name,
    menu_item_original_name = this.menuItemOriginalName,
    id = this.id!!
)

fun UserReviewNote.mapToEntity() = UserReviewNoteEntity(
    id = this.id,
    menu_item_id = this.menuItemId,
    relative_time_description = this.relativeTimeDescription,
    text = this.text,
    last_change = this.lastChanged,
)

fun UserReviewNoteEntity.mapToDomain() = UserReviewNote(
    id = this.id,
    menuItemId = this.menu_item_id,
    relativeTimeDescription = this.relative_time_description,
    text = this.text,
    lastChanged = this.last_change,
)

//Only when the data is complete and there is no more to fetch
fun Restaurant.mapToCompleteEntity() = PlaceCompleteEntity(
    placeEntity = this.mapToEntity(true),
    openHours = this.openHours?.periods?.map { it.mapToEntity(restaurantId = this.id) },
)

fun Photo.mapToEntity(restaurantId: String) = PlacePhotoEntity(
    height = this.height,
    width = this.width,
    photo_reference = this.photoReference,
    place_id = restaurantId
)

fun DayPeriod.mapToEntity(restaurantId: String) = DayPeriodEntity(
    place_id = restaurantId,
    open_day = this.open.day,
    open_time = this.open.time,
    close_day = this.close?.day,
    close_time = this.close?.time,

    )

/**
 * To use when data is not complete
 */
fun Restaurant.mapToEntity(isCompleteData: Boolean? = null) = PlaceEntity(
    favorite = this.favorite,
    mainPhoto = this.mainPhoto,
    business_status = this.businessStatus,
    formatted_address = this.formattedAddress,
    formatted_phone_number = this.formattedPhoneNumber,
    user_rating = this.userRating,
    location =
    LocationEntity(
        this.location.lat,
        this.location.lng
    ),
    icon = this.icon,
    international_phone_number = this.internationalPhoneNumber,
    name = this.name,
    place_id = this.id,
    price_level = this.priceLevel,
    google_rating = this.googleRating,
    reference = this.reference,
    scope = this.scope,
    user_ratings_total = this.userRatingsTotal,
    vicinity = this.vicinity,
    url = this.url,
    website = this.website,
    last_changed = this.lastChanged,
    is_complete_data = isCompleteData ?: this.isCompleteData,
)

fun SearchCacheEntity.Companion.createEntity(
    text_query: String = "",
    distance_in_meters: Long = 0,
    position_code: String = "",
    types: String,
    created_millis: Long,
) = SearchCacheEntity(
    id = createKey(text_query, distance_in_meters.toString(), position_code, types),
    text_query = text_query,
    distance_in_meters = distance_in_meters,
    position_code = position_code,
    types = types,
    created_millis = created_millis,
)

fun createKey(vararg keys: String?): String = keys.joinToString()
