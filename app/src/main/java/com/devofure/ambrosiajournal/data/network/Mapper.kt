package com.devofure.ambrosiajournal.data.network

import com.devofure.ambrosiajournal.domain.*

fun PlaceNM.mapToDomain(cacheTime: Long, isCompleteData: Boolean) = Restaurant(
    favorite = null,
    mainPhoto = this.photos?.first()?.photo_reference,
    businessStatus = this.business_status,
    formattedAddress = this.formatted_address,
    formattedPhoneNumber = this.formatted_phone_number,
    location = Location(this.geometry.location.lat, this.geometry.location.lng),
    icon = this.icon,
    internationalPhoneNumber = this.international_phone_number,
    name = this.name,
    openHours = if (this.opening_hours?.periods?.isNullOrEmpty() == false)
        OpenHours(
            this.opening_hours.open_now,
            this.opening_hours.periods.mapNotNull { if (it.open?.day != null) it.mapToDomain() else null },
            this.opening_hours.weekday_text
        ) else null,
    photos = this.photos?.map {
        Photo(
            height = it.height,
            width = it.width,
            photoReference = it.photo_reference,
            localPah = null,
            parentId = this.place_id,
        )
    } ?: emptyList(),
    id = this.place_id,
    priceLevel = this.price_level,
    googleRating = this.rating,
    userRating = null,
    reference = this.reference,
    reviews = this.reviews?.map {
        GoogleReview(
            it.author_name,
            it.author_url,
            it.language,
            it.profile_photo_url,
            it.rating,
            it.relative_time_description,
            it.text,
            it.time
        )
    } ?: emptyList(),
    scope = this.scope,
    types = this.types,
    userRatingsTotal = this.user_ratings_total,
    vicinity = this.vicinity,
    url = this.url,
    website = this.website,
    lastChanged = cacheTime,
    isCompleteData = isCompleteData
)

fun DayPeriodNM.mapToDomain() = DayPeriod(
    this.open.run {
        DayPeriodLimit(this!!.day, this.time)
    },
    this.close?.run {
        DayPeriodLimit(this.day, this.time)
    },
)