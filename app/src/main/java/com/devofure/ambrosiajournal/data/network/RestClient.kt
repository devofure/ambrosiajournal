@file:Suppress("SpellCheckingInspection", "unused", "unused")

package com.devofure.ambrosiajournal.data.network

import retrofit2.http.GET
import retrofit2.http.Query

sealed class InputType(val value: String) {
    class TextQuery : InputType("textquery")
    class Phonenumber : InputType("phonenumber")
}

interface PlacesRestClientService {
    //https://maps.googleapis.com/maps/api/place/findplacefromtext/output?parameters
    @GET("textsearch/json")
    suspend fun findPlaceFromText(
        @Query("input") input: String,
        @Query("inputtype") inputType: String,
        @Query("type") type: String,
        @Query("key") key: String,
    ): PlaceListResponse?

    //https://maps.googleapis.com/maps/api/place/textsearch/output?parameters
    @GET("details/json")
    suspend fun fetchPlaceDetails(
        @Query("place_id") placeId: String,
        @Query("fields") fields: String,
        @Query("key") key: String,
    ): PlaceResponse?

    //https://maps.googleapis.com/maps/api/place/nearbysearch/output?parameters
    @GET("nearbysearch/json")
    suspend fun fetchPlacesNearby(
        @Query("location") location: String,
        @Query("type") type: String,
        @Query("radius") radius: Long,
        @Query("key") key: String,
    ): PlaceListResponse?
}