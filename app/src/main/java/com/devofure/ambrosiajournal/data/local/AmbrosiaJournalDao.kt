package com.devofure.ambrosiajournal.data.local

import androidx.room.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged


@Dao
abstract class PlaceDao : BaseDao<PlaceEntity> {

    @Query("SELECT place_id FROM place WHERE place_id = :id")
    abstract suspend fun existPlace(id: String): String?

    @Query("SELECT * FROM place")
    abstract fun getPlaces(): Flow<List<PlaceEntity>>

    @Query("SELECT * FROM place WHERE place_id = :id")
    abstract suspend fun getPlace(id: String): PlaceEntity

    @Query("SELECT * FROM place WHERE favorite = 1")
    abstract fun getBookmarkedPlaces(): Flow<List<PlaceEntity>>

    @Update(entity = PlaceEntity::class)
    abstract suspend fun updateRate(placeRate: UpdatePlaceRateEntity)

    @ExperimentalCoroutinesApi
    fun getBookmarkPlaceDistinct() = getBookmarkedPlaces().distinctUntilChanged()

    @Transaction
    @Query("SELECT * FROM place WHERE place_id = :id")
    protected abstract fun getPlaceCompleteById(id: String): Flow<PlaceCompleteEntity?>

    @Transaction
    @Query("SELECT * FROM place WHERE place_id = :id")
    abstract suspend fun getPlaceCompleteByIdOnce(id: String): PlaceCompleteEntity?

    @ExperimentalCoroutinesApi
    fun getPlaceCompleteByIdDistinct(id: String) =
        getPlaceCompleteById(id).distinctUntilChanged()

    @Transaction
    @Query("SELECT * FROM place WHERE place_id = :id")
    abstract suspend fun loadPlaceByIdOnce(id: String): PlaceCompleteEntity?

    @Transaction
    open suspend fun upsertPlaceCompleteEntity(vararg places: PlaceCompleteEntity) {
        places.forEach { upsertEntity(it.placeEntity) }
    }

    @Transaction
    open suspend fun upsertPlace(vararg places: PlaceEntity) {
        places.forEach { upsertEntity(it) }
    }

    private suspend fun upsertEntity(placeEntity: PlaceEntity) {
        if (existPlace(placeEntity.place_id) != null) {
            val entity = getPlace(placeEntity.place_id)
            placeEntity.mergeUserData(entity)
            update(placeEntity)
        } else insertElseIgnore(placeEntity)
    }

    @Update(entity = PlaceEntity::class)
    abstract suspend fun updateFavorite(place: UpdatePlaceFavoriteEntity)
}

@Dao
abstract class MenuItemDao : BaseDao<MenuItemEntity> {

    @Update(entity = MenuItemEntity::class)
    abstract suspend fun updateFavorite(updateMenuItemFavoriteEntity: UpdateMenuItemFavoriteEntity)

    @Update(entity = MenuItemEntity::class)
    abstract suspend fun updateRate(updateMenuItemRateEntity: UpdateMenuItemRateEntity)

    @Transaction
    @Query("SELECT * FROM menu_item WHERE place_id = :placeId")
    protected abstract fun getPlaceMenuById(placeId: String): Flow<List<MenuItemCompleteEntity>?>

    @ExperimentalCoroutinesApi
    fun loadDistinctPlaceMenuById(placeId: String) =
        getPlaceMenuById(placeId).distinctUntilChanged()

    @Transaction
    @Query("SELECT * FROM menu_item WHERE favorite = 1")
    abstract fun getBookmarkedMenuItem(): Flow<List<MenuItemCompleteEntity>>

    @ExperimentalCoroutinesApi
    fun getBookmarkMenuItemDistinct() = getBookmarkedMenuItem().distinctUntilChanged()

    @Transaction
    @Query("SELECT * FROM menu_item WHERE id = :id")
    protected abstract fun getMenuItemCompleteById(id: String): Flow<MenuItemCompleteEntity?>

    @ExperimentalCoroutinesApi
    fun getMenuItemCompleteByIdDistinct(id: String) =
        getMenuItemCompleteById(id).distinctUntilChanged()

    @Update(entity = MenuItemEntity::class, onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun updateName(place: UpdateMenuItemNameEntity)
}

@Dao
abstract class UserReviewNoteDao : BaseDao<UserReviewNoteEntity> {

    @Query("SELECT * FROM user_review_note WHERE menu_item_id = :menuItemId")
    protected abstract fun getReviewNotesByMenuItemId(menuItemId: String): Flow<List<UserReviewNoteEntity>?>

    @ExperimentalCoroutinesApi
    fun loadDistinctReviewNotesByMenuItemId(menuItemId: String): Flow<List<UserReviewNoteEntity>?> =
        getReviewNotesByMenuItemId(menuItemId).distinctUntilChanged()

    @Query("SELECT * FROM user_review_note WHERE id = :userReviewNoteId")
    protected abstract fun getReviewNoteByMenuItemId(userReviewNoteId: String): Flow<UserReviewNoteEntity?>

    @ExperimentalCoroutinesApi
    fun loadDistinctReviewNoteByMenuItemId(userReviewNoteId: String): Flow<UserReviewNoteEntity?> =
        getReviewNoteByMenuItemId(userReviewNoteId).distinctUntilChanged()
}

@Dao
abstract class DayPeriodDao : BaseDao<DayPeriodEntity> {

    @Query("DELETE FROM day_period WHERE place_id = :placeId")
    abstract suspend fun deleteByPlace(placeId: String)

    @Transaction
    open suspend fun replace(vararg entity: DayPeriodEntity) {
        entity.forEach { deleteByPlace(it.place_id) }
        insertAndReplace(*entity)
    }
}

@Dao
abstract class PlacePhotoDao : BaseDao<PlacePhotoEntity> {

    @Query("DELETE FROM photos WHERE place_id = :placeId")
    abstract suspend fun deleteByPlace(placeId: String)

    @Transaction
    open suspend fun replace(vararg entity: PlacePhotoEntity) {
        entity.forEach { deleteByPlace(it.place_id) }
        insertAndReplace(*entity)
    }

    @Query("SELECT * FROM photos WHERE place_id = :placeId")
    protected abstract fun loadPlacePhotos(placeId: String): Flow<List<PlacePhotoEntity>?>

    fun loadDistinctPlacePhotos(placeId: String): Flow<List<PlacePhotoEntity>?> =
        loadPlacePhotos(placeId).distinctUntilChanged()
}

@Dao
abstract class MenuItemPhotoDao : BaseDao<MenuItemPhotoEntity> {

    @Query("SELECT image_url FROM menu_item WHERE id = :id")
    abstract suspend fun existMainPhoto(id: String): String?

    @Query("SELECT * FROM menu_item_photos WHERE menu_item_id = :menuItemId")
    abstract fun loadMenuItemPhotos(menuItemId: String): Flow<List<MenuItemPhotoEntity>?>

    fun loadDistinctMenuItemPhotos(menuItemId: String): Flow<List<MenuItemPhotoEntity>?> =
        loadMenuItemPhotos(menuItemId).distinctUntilChanged()

    @Update(entity = MenuItemEntity::class)
    abstract suspend fun insertOrIgnoreMainPhoto(updateMenuItemMainImageEntity: UpdateMenuItemMainImageEntity)

    @Transaction
    open suspend fun insertOrIgnorePhoto(menuItemId: String, mainImage: String) {
        if (existMainPhoto(menuItemId) == null) {
            insertOrIgnoreMainPhoto(
                UpdateMenuItemMainImageEntity(menuItemId, mainImage))
        }
    }
}

@Dao
abstract class GoogleReviewDao : BaseDao<GoogleReviewEntity>

@Dao
abstract class SearchCacheDao : BaseDao<SearchCacheEntity> {

    @Transaction
    @Query("SELECT * FROM search_cache WHERE distance_in_meters= :distanceInMeters AND types= :type AND position_code = :positionCode")
    abstract suspend fun loadSearchNearbyCache(
        distanceInMeters: Long,
        type: String,
        positionCode: String,
    ): SearchCacheCompleteEntity?

    @Transaction
    @Query("SELECT * FROM search_cache WHERE text_query= :textQuery AND types= :type")
    abstract suspend fun loadSearchQueryCache(
        textQuery: String,
        type: String,
    ): SearchCacheCompleteEntity?
}

@Dao
abstract class SearchPlaceCacheDao : BaseDao<SearchPlaceCacheEntity>