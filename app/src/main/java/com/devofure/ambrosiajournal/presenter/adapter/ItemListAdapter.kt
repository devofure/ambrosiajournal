package com.devofure.ambrosiajournal.presenter.adapter

import android.view.*
import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.selection.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

sealed class ItemListStateMode {
    object SelectionMode : ItemListStateMode()
    object DefaultMode : ItemListStateMode()
}

abstract class ItemListAdapter<I : DomainModel, VB : ViewDataBinding>(
    private val itemResId: Int,
) : ListAdapter<I, ItemListAdapter<I, VB>.ItemViewHolder>(DiffCallback()) {
    //multi select
    private var itemListStateMode: ItemListStateMode = ItemListStateMode.DefaultMode
    private lateinit var keyProvider: ItemIdKeyProvider
    private var tracker: SelectionTracker<String>? = null

    abstract fun onBindItemCreated(itemViewHolder: ItemViewHolder)

    abstract fun onBindItem(itemBinding: VB, eventData: BindingEventData<I>)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), itemResId, parent, false)
        ).also { onBindItemCreated(it) }
    }


    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val model = getItem(position)
        val isSelected: Boolean = tracker?.isSelected(model.id) ?: false
        holder.bind(model, position, isSelected, itemCount)
    }

    data class BindingEventData<I>(
        val item: I,
        val isSelected: Boolean,
        val position: Int,
        val totalSize: Int,
    )

    private fun retrieveTracker(
        recyclerView: RecyclerView,
        selectTrackerId: String,
        keyProvider: ItemIdKeyProvider,
    ): SelectionTracker<String> =
        SelectionTracker.Builder(
            selectTrackerId,
            recyclerView,
            keyProvider,
            ItemLookup(recyclerView),
            StorageStrategy.createStringStorage(),
        )
            .withSelectionPredicate(SelectionPredicates.createSelectAnything())
            .build()

    inner class ItemViewHolder(val binding: VB) : RecyclerView.ViewHolder(binding.root) {

        private val _itemDetails = ClickAndDragItemDetails()
        val itemDetails: ItemDetailsLookup.ItemDetails<String> = _itemDetails

        fun bind(item: I, position: Int, isSelected: Boolean, totalSize: Int) {
            _itemDetails.setIdentifier(item.id!!)
            _itemDetails.position = position
            onBindItem(binding, BindingEventData(item, isSelected, position, totalSize))
            binding.executePendingBindings()
        }
    }

    class ClickAndDragItemDetails : ItemDetailsLookup.ItemDetails<String>() {
        private var _position = 0
        private var _identifier: String? = null

        fun setIdentifier(key: String) {
            _identifier = key
        }

        override fun getPosition(): Int = _position

        fun setPosition(position: Int) {
            _position = position
        }

        @Nullable
        override fun getSelectionKey(): String? = _identifier

        override fun inSelectionHotspot(e: MotionEvent): Boolean = true

        override fun inDragRegion(e: MotionEvent): Boolean = true
    }

    inner class ItemIdKeyProvider(private val recyclerView: RecyclerView) :
        ItemKeyProvider<String>(SCOPE_MAPPED) {
        private var mKeyToPosition: HashMap<String, Int> = HashMap(0)

        override fun getKey(position: Int): String {
            return (recyclerView.adapter as ItemListAdapter<*, *>).currentList[position].id
                ?: throw IllegalStateException("RecyclerView adapter is not set!")
        }

        override fun getPosition(key: String): Int {
            return mKeyToPosition.getOrDefault(key, RecyclerView.NO_POSITION)
        }
    }

    class ItemLookup(private val rv: RecyclerView) : ItemDetailsLookup<String>() {
        override fun getItemDetails(event: MotionEvent)
                : ItemDetails<String>? {

            val view = rv.findChildViewUnder(event.x, event.y)
            if (view != null) {
                return (rv.getChildViewHolder(view) as ItemListAdapter<*, *>.ItemViewHolder).itemDetails
            }
            return null
        }
    }

    //Selection
    fun setupMultiSelect(
        recyclerView: RecyclerView,
        toolbar: Toolbar,
        menuResId: Int,
        onActionItemSelectionClicked: (actionMode: ActionMode?, menuItem: MenuItem?, SelectionTracker<String>?) -> Boolean,
        selectTrackerId: String,
    ) {
        keyProvider = ItemIdKeyProvider(recyclerView)
        this.tracker = retrieveTracker(recyclerView, selectTrackerId, keyProvider)
        this.tracker!!.addObserver(object : SelectionTracker.SelectionObserver<String?>() {
            override fun onSelectionChanged() {
                super.onSelectionChanged()
                if (tracker!!.hasSelection() && itemListStateMode is ItemListStateMode.DefaultMode) {
                    itemListStateMode = ItemListStateMode.SelectionMode
                    toolbar.startActionMode(
                        createActionModeCallback(tracker!!, menuResId, onActionItemSelectionClicked)
                    )
                }
            }
        })
    }

    private fun createActionModeCallback(
        currentTracker: SelectionTracker<String>,
        menuResId: Int,
        onActionModeSelectedListener: (actionMode: ActionMode?, menuItem: MenuItem?, SelectionTracker<String>?) -> Boolean,
    ): ActionMode.Callback =
        object : ActionMode.Callback {
            override fun onCreateActionMode(actionMode: ActionMode?, menu: Menu?): Boolean {
                actionMode?.menuInflater?.inflate(menuResId, menu)
                actionMode?.title = currentTracker.selection.size().toString()
                currentTracker.addObserver(object : SelectionTracker.SelectionObserver<String?>() {
                    override fun onSelectionChanged() {
                        //finish actionMode when there is no selection
                        if (itemListStateMode is ItemListStateMode.SelectionMode)
                            if (currentTracker.selection.size() == 0) actionMode?.finish()
                            else actionMode?.title = currentTracker.selection.size().toString()
                    }
                })
                return true
            }

            override fun onPrepareActionMode(p0: ActionMode?, p1: Menu?): Boolean = false

            override fun onActionItemClicked(actionMode: ActionMode?, menuItem: MenuItem?) =
                onActionModeSelectedListener.invoke(actionMode, menuItem, tracker!!)

            override fun onDestroyActionMode(p0: ActionMode?) {
                if (currentTracker.selection.size() > 0) {
                    currentTracker.clearSelection()
                }
                itemListStateMode = ItemListStateMode.DefaultMode
            }
        }
}

class DiffCallback<I : DomainModel> : DiffUtil.ItemCallback<I>() {

    override fun areItemsTheSame(oldItem: I, newItem: I): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: I, newItem: I): Boolean {
        return oldItem.isSameContent(newItem)
    }
}

interface DomainModel {
    val id: String?
    fun <C : DomainModel> isSameContent(other: C): Boolean = this == other
}