package com.devofure.ambrosiajournal.presenter.ui.place

import androidx.lifecycle.*
import com.devofure.ambrosiajournal.data.AmbrosiaJournalRepository
import com.devofure.ambrosiajournal.domain.DataResult
import com.devofure.ambrosiajournal.domain.MenuItem
import com.devofure.ambrosiajournal.domain.Photo
import com.devofure.ambrosiajournal.domain.Restaurant
import com.devofure.ambrosiajournal.presenter.adapter.DomainModel
import com.devofure.ambrosiajournal.presenter.base.BaseViewModel
import com.devofure.ambrosiajournal.presenter.base.Event
import com.devofure.ambrosiajournal.presenter.base.EventProcess.Finish
import com.devofure.ambrosiajournal.presenter.base.EventProcess.InProcess
import com.devofure.ambrosiajournal.presenter.base.EventProcessKey.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

inline class RestaurantKey(val value: String)

@HiltViewModel
class RestaurantViewModel
@Inject constructor(
    private val repository: AmbrosiaJournalRepository,
) : BaseViewModel() {

    private val _restaurantChannel = MutableLiveData<RestaurantKey>()
    private val _restaurantChannelUnique = _restaurantChannel.distinctUntilChanged()

    val rateSelected = MutableLiveData(1.0f)

    fun notifyRestaurantId(restaurantId: String) {
        viewModelScope.launch {
            _restaurantChannel.value = RestaurantKey(restaurantId)
        }
    }

    fun notifyRestaurantIdWithSync(placeId: String, force: Boolean = false) {
        viewModelScope.launch {
            //strategy to make it impossible to have a infinity network fetch loop
            val processKey = if (force) UserSyncPlaceProcess else SyncRestaurantProcess
            runEventProcess(
                eventKey = processKey,
                actionEvent = { repository.syncPlace(placeId, force) },
                handleExceptionEvent = { t -> notifyCommonExceptions(t, processKey) }
            )
            _restaurantChannel.value = RestaurantKey(placeId)
        }
    }

    val restaurant: LiveData<DataResult<Restaurant>> =
        _restaurantChannelUnique.switchMap { restaurantKey: RestaurantKey ->
            repository.retrievePlace(restaurantKey.value)
                .onStart { _eventProcess.value = Event(InProcess(LoadRestaurantProcess)) }
                .catch { notifyCommonExceptions(it, LoadRestaurantProcess) }
                .asLiveData()
        }


    val restaurantMenu: LiveData<DataResult<List<MenuItem>>> =
        _restaurantChannelUnique.switchMap { restaurantKey: RestaurantKey ->
            repository.retrieveRestaurantMenu(restaurantKey.value)
                .onStart { _eventProcess.value = Event(InProcess(LoadMenuItemProcess)) }
                .catch { notifyCommonExceptions(it, LoadMenuItemProcess) }
                .onCompletion { _eventProcess.value = Event(Finish(LoadMenuItemProcess)) }
                .asLiveData()
        }

    val restaurantPhotos: LiveData<DataResult<List<Photo>>> =
        _restaurantChannelUnique.switchMap { restaurantKey: RestaurantKey ->
            repository.retrieveRestaurantPhotos(restaurantKey.value)
                .onStart { _eventProcess.value = Event(InProcess(LoadMenuItemPhotosProcess)) }
                .catch {
                    notifyCommonExceptions(it, LoadMenuItemPhotosProcess)
                }
                .onCompletion { _eventProcess.value = Event(Finish(LoadMenuItemPhotosProcess)) }
                .asLiveData()
        }

    fun <I : DomainModel> switchBookmark(item: I) {
        when (item) {
            is Restaurant -> viewModelScope.launch {
                val newFavoriteValue = item.favorite != true
                item.favorite = newFavoriteValue
                repository.updateRestaurantFavorite(item.id, newFavoriteValue)
            }
            is MenuItem -> viewModelScope.launch {
                val switchFavoriteValue = item.favorite != true
                if (item.id != null)
                    repository.updateMenuItemFavorite(item.id!!, switchFavoriteValue)
            }
        }
    }

    fun rateRestaurant(rate: Int) {
        val dataResult = restaurant.value
        if (dataResult is DataResult.Success) {
            val restaurantItem = dataResult.data
            viewModelScope.launch {
                runEventProcess(
                    RateRestaurantProcess,
                    actionEvent = {
                        repository.updateRestaurantRate(restaurantItem.id, rate)
                    },
                    handleExceptionEvent = { t -> notifyCommonExceptions(t, RateRestaurantProcess) }
                )
            }
        }
    }
}