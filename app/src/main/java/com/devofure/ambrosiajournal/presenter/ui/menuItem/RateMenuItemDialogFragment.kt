package com.devofure.ambrosiajournal.presenter.ui.menuItem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.databinding.DialogFragmentRateMenuItemBinding
import com.devofure.ambrosiajournal.domain.DataResult
import com.devofure.ambrosiajournal.presenter.NavigationDestination
import com.devofure.ambrosiajournal.presenter.NavigationViewModel
import com.devofure.ambrosiajournal.presenter.base.EventProcess
import com.devofure.ambrosiajournal.presenter.base.EventProcessKey
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RateMenuItemDialogFragment : DialogFragment() {

    private val args: RateMenuItemDialogFragmentArgs by navArgs()
    private val menuItemViewModel: MenuItemViewModel by viewModels()
    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.mobile_navigation)

    init {
        setStyle(STYLE_NORMAL, R.style.Theme_AmbrosiaJournal_FullScreenDialogTheme)
    }

    override fun onStart() {
        super.onStart()
        menuItemViewModel.setMenuItemId(args.menuItemKey)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        val binding: DialogFragmentRateMenuItemBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_fragment_rate_menu_item,
            container,
            false
        )
        bindViews(binding)
        setupNavigation()
        setupEventProcess()
        return binding.root
    }

    private fun setupEventProcess() {
        menuItemViewModel.eventProcess.observe(viewLifecycleOwner, {
            val eventProcess = it.peek()
            if (eventProcess is EventProcess.Finish) {
                if (eventProcess.eventKey is EventProcessKey.RateMenuItemProcess && menuItemViewModel.rateMenuItemActionByUser) {
                    dialog?.cancel()
                }
            }
        })
    }

    private fun bindViews(binding: DialogFragmentRateMenuItemBinding) {
        binding.viewModel = menuItemViewModel
        binding.navigationViewModel = navigationViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        menuItemViewModel.rateSelected.observe(viewLifecycleOwner, {
            binding.rateSelected = it.toInt()
        })
        menuItemViewModel.menuItem.observe(viewLifecycleOwner, {
            if (it is DataResult.Success) {
                binding.menuItemViewModelItem = it.data
                menuItemViewModel.rateSelected.value = it.data.userRating?.toFloat() ?: 1.0f
            } else if (it is DataResult.Error) {
                it.exception.printStackTrace()
            }
        })
    }

    private fun setupNavigation() {
        navigationViewModel.newDestination.observe(viewLifecycleOwner, Observer { event ->
            when (event.peek()) {
                is NavigationDestination.NavigationBack -> {
                    if (event.consume() == null) return@Observer
                    dialog?.cancel()
                }
                else -> {
                }
            }
        })
    }
}