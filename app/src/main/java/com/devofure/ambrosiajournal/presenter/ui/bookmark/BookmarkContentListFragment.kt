package com.devofure.ambrosiajournal.presenter.ui.bookmark

import android.Manifest
import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.doOnPreDraw
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.navGraphViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.databinding.FragmentListBinding
import com.devofure.ambrosiajournal.domain.DataResult
import com.devofure.ambrosiajournal.domain.MenuItem
import com.devofure.ambrosiajournal.domain.Restaurant
import com.devofure.ambrosiajournal.hardware.LocationRetriever
import com.devofure.ambrosiajournal.presenter.NavigationViewModel
import com.devofure.ambrosiajournal.presenter.adapter.MenuItemListAdapter
import com.devofure.ambrosiajournal.presenter.adapter.PlaceItemListAdapter
import com.devofure.ambrosiajournal.presenter.base.EventProcess
import com.devofure.ambrosiajournal.presenter.base.withPermissions
import com.devofure.ambrosiajournal.presenter.navigateNowToMenuItemDetailWithTransition
import com.devofure.ambrosiajournal.presenter.navigateNowToPlaceDetailsWithTransition
import com.devofure.ambrosiajournal.presenter.ui.menuItem.MenuItemViewModel
import com.devofure.ambrosiajournal.presenter.ui.place.RestaurantViewModel
import com.devofure.ambrosiajournal.presenter.ui.search.SearchFragment
import com.devofure.ambrosiajournal.presenter.ui.search.SearchViewState
import com.google.android.material.transition.MaterialElevationScale
import com.vmadalin.easypermissions.EasyPermissions
import com.vmadalin.easypermissions.annotations.AfterPermissionGranted
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

sealed class BookmarkItemType(val value: Int) {
    object MenuItem : BookmarkItemType(0)
    object Restaurant : BookmarkItemType(1)
}

const val ARG_ITEM_TYPE = "bookmark.ITEM_TYPE"

@AndroidEntryPoint
class BookmarkContentListFragment : Fragment() {

    companion object {
        private const val REQUEST_CODE_LOCATION: Int = 55621
    }

    private lateinit var placeAdapterList: PlaceItemListAdapter
    private lateinit var menuItemAdapterList: MenuItemListAdapter
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var itemType: BookmarkItemType
    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.mobile_navigation)
    private val placeViewModel: RestaurantViewModel by activityViewModels()
    private val menuItemViewModel: MenuItemViewModel by viewModels()
    private val viewModel: BookmarkViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        val binding: FragmentListBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_list,
            container,
            false
        )
        swipeRefreshLayout = binding.swipeRefreshLayout
        //disable until the recycler places will activate it
        swipeRefreshLayout.isEnabled = false
        setupRecyclerview(binding, viewModel, navigationViewModel)
        setupLoadingView()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Postpone enter transitions to allow shared element transitions to run.
        // https://github.com/googlesamples/android-architecture-components/issues/495
        postponeEnterTransition()
        view.doOnPreDraw { startPostponedEnterTransition() }
    }

    private fun loadSetupSettings() {
        val typeIdInt = arguments?.getInt(ARG_ITEM_TYPE) ?: 0
        itemType =
            if (typeIdInt == BookmarkItemType.MenuItem.value) BookmarkItemType.MenuItem
            else BookmarkItemType.Restaurant
    }

    private fun setupRecyclerview(
        binding: FragmentListBinding,
        viewModel: BookmarkViewModel,
        navigationViewModel: NavigationViewModel,
    ) {
        loadSetupSettings()
        return when (itemType) {
            BookmarkItemType.Restaurant -> setupRestaurantRecyclerView(
                binding,
                viewModel,
                navigationViewModel
            )
            BookmarkItemType.MenuItem -> setupMenuItemRecyclerView(binding,
                viewModel,
                navigationViewModel)
        }
    }

    private fun setupMenuItemRecyclerView(
        binding: FragmentListBinding,
        viewModel: BookmarkViewModel,
        navigationViewModel: NavigationViewModel,
    ) {
        menuItemAdapterList = menuItemListAdapter()

        binding.list.run {
            layoutManager = GridLayoutManager(context,
                requireContext().resources.getInteger(R.integer.menu_item_list_span))
            adapter = menuItemAdapterList
        }
        viewModel.bookmarkedMenuItems.observe(viewLifecycleOwner, {
            if (it is DataResult.Success) {
                val textTitle = it.data.size
                binding.listTitle = "$textTitle Menu Item/s"
                menuItemAdapterList.submitList(it.data)
            }
        })
    }

    private fun menuItemListAdapter(): MenuItemListAdapter {
        return MenuItemListAdapter(menuItemAdapterListener())
    }

    private fun menuItemAdapterListener(): MenuItemListAdapter.MenuItemAdapterListener {
        return object : MenuItemListAdapter.MenuItemAdapterListener {
            override fun onClicked(cardView: View, menuItem: MenuItem) {
                setupTransitionToDetailFragment()
                cardView.navigateNowToMenuItemDetailWithTransition(menuItem)
            }

            override fun onStarChanged(menuItem: MenuItem, newValue: Boolean, position: Int) {
                menuItemViewModel.switchBookmark(menuItem)
                menuItemAdapterList.notifyItemChanged(position)
            }
        }
    }

    private fun setupRestaurantRecyclerView(
        binding: FragmentListBinding,
        viewModel: BookmarkViewModel,
        navigationViewModel: NavigationViewModel,
    ) {
        placeAdapterList = placeItemListAdapter()
        binding.list.run {
            layoutManager = GridLayoutManager(context,
                requireContext().resources.getInteger(R.integer.place_list_span))
            adapter = placeAdapterList
        }
        observeBookmarkedPlaces(viewModel, binding, placeAdapterList)

        setupSwipeRefreshLayout(placeAdapterList)
    }

    private fun placeItemListAdapter(): PlaceItemListAdapter {
        return PlaceItemListAdapter(itemListener = placeAdapterListener())
    }

    private fun placeAdapterListener(): PlaceItemListAdapter.PlaceAdapterListener {
        return object :
            PlaceItemListAdapter.PlaceAdapterListener {
            override fun onClicked(cardView: View, place: Restaurant) {
                setupTransitionToDetailFragment()
                cardView.navigateNowToPlaceDetailsWithTransition(place)
            }

            override fun onStarChanged(place: Restaurant, newValue: Boolean, position: Int) {
                placeViewModel.switchBookmark(place)
                placeAdapterList.notifyItemChanged(position)
            }
        }
    }

    private fun setupTransitionToDetailFragment() {
        val motionDuration = resources.getInteger(R.integer.motion_duration_extra_large).toLong()
        exitTransition = MaterialElevationScale(false).apply { duration = motionDuration }
        reenterTransition = MaterialElevationScale(true).apply { duration = motionDuration }
    }

    private fun observeBookmarkedPlaces(
        viewModel: BookmarkViewModel,
        binding: FragmentListBinding,
        adapterList: PlaceItemListAdapter,
    ) {
        viewModel.bookmarkedPlaces.observe(viewLifecycleOwner, {
            if (it is DataResult.Success) {
                val textTitle = it.data.size
                binding.listTitle = "$textTitle Places"
                adapterList.submitList(it.data)
            }
        })
    }

    private fun setupSwipeRefreshLayout(
        adapterList: PlaceItemListAdapter,
    ) {
        swipeRefreshLayout.isEnabled = false
        swipeRefreshLayout.setOnRefreshListener { updateLocationWithPermission(adapterList) }
    }

    @AfterPermissionGranted(REQUEST_CODE_LOCATION)
    private fun updateLocationWithPermission(adapterList: PlaceItemListAdapter) =
        withPermissions(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)
            .runIfPermitted { updateLocation(adapterList) }
            .execute()

    private fun updateLocation(
        adapterList: PlaceItemListAdapter,
    ) {
        viewLifecycleOwner.lifecycleScope.launch {
            val location = LocationRetriever(requireContext().applicationContext).getLocation()
            location?.let {
                viewModel.setLocation(location)
                updateAdapterList(adapterList)
            }
        }
    }

    private fun updateAdapterList(adapterList: PlaceItemListAdapter) =
        viewLifecycleOwner.lifecycleScope.launch {
            val sortedPlaces = viewModel.sortByLocation(adapterList.currentList)
            withContext(Dispatchers.Main) {
                adapterList.submitList(sortedPlaces) {
                    if (adapterList.itemCount >= 1)
                        adapterList.notifyItemRangeChanged(0, adapterList.itemCount)
                }
                stopShowingLoadingView()
            }
        }

    private fun setupLoadingView() {
        viewModel.eventProcess.observe(viewLifecycleOwner, {
            when (it.peek()) {
                is EventProcess.InProcess -> {
                    showLoadingView()
                }
                is EventProcess.Finish -> {
                    stopShowingLoadingView()
                }
                is EventProcess.Error -> {
                    stopShowingLoadingView()
                }
            }
        })
    }

    private fun showLoadingView() {
        swipeRefreshLayout.isRefreshing = true
    }

    private fun stopShowingLoadingView() {
        swipeRefreshLayout.isRefreshing = false
    }
}