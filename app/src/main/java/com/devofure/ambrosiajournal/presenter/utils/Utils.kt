package com.devofure.ambrosiajournal.presenter.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.ceil

fun showSoftKeyboard(view: View, activity: Activity) {
    view.isFocusable = true
    if (view.requestFocus()) {
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }
}

fun hideKeyboard(context: Context, view: View) {
    view.clearFocus()
    val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun toEmptyIfNull(value: String?): String = value ?: ""

fun intToString(value: Int?, fallback: String): String =
    toEmptyIfNull(value?.toString() ?: fallback)

fun metersToUiString(meters: Double?): String {
    return if (meters != null) {
        if (meters < 1000)
            ceil(meters)
                .toInt()
                .toString() + " m"
        else
            (meters / 1000)
                .toBigDecimal()
                .setScale(1, RoundingMode.HALF_UP)
                .toString() + " Km"
    } else
        ""
}

fun toTimestamp(value: Long): String {
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
    val date = Date(value)
    return simpleDateFormat.format(date)
}

fun goToPhoneToCall(contest: Context, phoneNumber: String) {
    val callIntent = Intent(Intent.ACTION_DIAL)
    callIntent.data = Uri.parse("tel:$phoneNumber")
    contest.startActivity(callIntent)
}

fun goToLink(contest: Context, link: String) {
    val i = Intent(Intent.ACTION_VIEW, Uri.parse(link))
    contest.startActivity(i)
}

fun sendEmail(contest: Context, emailTo: String, subject: String, message: String? = null) {
    val intent = Intent(
        Intent.ACTION_SENDTO, Uri.fromParts(
            "mailto", emailTo, null
        )
    )
    intent.putExtra(Intent.EXTRA_SUBJECT, subject)
    intent.putExtra(Intent.EXTRA_TEXT, message)
    contest.startActivity(Intent.createChooser(intent, ""))
}