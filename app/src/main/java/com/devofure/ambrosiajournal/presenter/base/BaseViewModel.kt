package com.devofure.ambrosiajournal.presenter.base

import androidx.lifecycle.MutableLiveData
import com.devofure.ambrosiajournal.data.NoInternetAvailable
import com.devofure.ambrosiajournal.data.UnknownProblem
import com.devofure.ambrosiajournal.presenter.base.EventProcess.Error
import java.net.UnknownHostException

/**
 * class that wraps the MutableLiveData to improve readability
 */
class EventProcessLiveData : MutableLiveData<Event<EventProcess<EventProcessKey>>>()
class EventRequestLiveData : MutableLiveData<Event<EventRequestKey>>()

open class BaseViewModel : EventProcessViewModel<EventProcessKey, EventRequestKey>(
    EventProcessLiveData(),
    EventRequestLiveData(),
) {

    internal fun notifyCommonExceptions(exception: Throwable, eventProcessKey: EventProcessKey) {
        if (exception is UnknownHostException) {
            _eventProcess.value = Event(Error(eventProcessKey, NoInternetAvailable("No Internet")))
        } else {
            _eventProcess.value =
                Event(Error(eventProcessKey, UnknownProblem("Something When wrong")))
        }
    }
}

/**
 * Event keys that defines the process that exists in the app
 */
sealed class EventProcessKey {
    object SaveMenuItemProcess : EventProcessKey()
    object RateMenuItemProcess : EventProcessKey()
    object LoadNearbyPlacesProcess : EventProcessKey()
    object LoadBookmarkedPlacesProcess : EventProcessKey()
    object LoadBookmarkedMenuItemProcess : EventProcessKey()
    object SearchPlacesProcess : EventProcessKey()
    object LoadRestaurantProcess : EventProcessKey()

    /**
     * When the place information refresh it's data if it is necessary
     */
    object SyncRestaurantProcess : EventProcessKey()

    /**
     * User force refresh place information
     */
    object UserSyncPlaceProcess : EventProcessKey()
    object LoadMenuItemProcess : EventProcessKey()
    object RateRestaurantProcess : EventProcessKey()
    object LoadReviewNoteProcess : EventProcessKey()
    object SaveReviewNoteProcess : EventProcessKey()
    object LoadMenuItemPhotosProcess : EventProcessKey()
}

sealed class EventRequestKey {
    data class UpdateItemInList(val currentPosition: Int) : EventRequestKey()
}