package com.devofure.ambrosiajournal.presenter.ui.search

import android.annotation.SuppressLint
import androidx.lifecycle.*
import com.devofure.ambrosiajournal.data.AmbrosiaJournalRepository
import com.devofure.ambrosiajournal.data.TYPE_CAFE
import com.devofure.ambrosiajournal.data.TYPE_DELIVERY
import com.devofure.ambrosiajournal.data.TYPE_RESTAURANT
import com.devofure.ambrosiajournal.domain.DataResult
import com.devofure.ambrosiajournal.domain.LocationGrid
import com.devofure.ambrosiajournal.domain.Restaurant
import com.devofure.ambrosiajournal.domain.createLocationGridOutOfDistance
import com.devofure.ambrosiajournal.presenter.base.BaseViewModel
import com.devofure.ambrosiajournal.presenter.base.Event
import com.devofure.ambrosiajournal.presenter.base.EventProcess.*
import com.devofure.ambrosiajournal.presenter.base.EventProcessKey
import com.devofure.ambrosiajournal.presenter.base.EventProcessKey.LoadNearbyPlacesProcess
import com.devofure.ambrosiajournal.presenter.base.EventProcessKey.SearchPlacesProcess
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

const val RADIUS_METER_SEARCH: Long = 2000

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val repository: AmbrosiaJournalRepository,
) : BaseViewModel() {

    private val restaurantNameChannel = MutableLiveData<String>()
    val searchQueryText: LiveData<String> = restaurantNameChannel
    private val currentPositionChannel = MutableLiveData<LocationGrid>()
    private val searchResultStateChannel = MutableLiveData<SearchViewState>()
    val searchResultState get() : LiveData<SearchViewState> = searchResultStateChannel

    private val _clearResult = MutableLiveData<Event<Int>>()
    val clearResult get() : LiveData<Event<Int>> = _clearResult

    @SuppressLint("NullSafeMutableLiveData")
    fun findRestaurantByName(query: String?) {
        if (query != null) {
            viewModelScope.launch {
                restaurantNameChannel.value = query
            }
        }
    }

    fun notifySearchViewState(searchViewState: SearchViewState) {
        searchResultStateChannel.value = searchViewState
    }

    val placeTextSearchList: LiveData<Event<DataResult<List<Restaurant>>>> =
        restaurantNameChannel.switchMap { searchQuery: String ->
            repository
                .retrieveRestaurantByName(searchQuery,
                    listOf(TYPE_RESTAURANT, TYPE_CAFE, TYPE_DELIVERY))
                .onStart { _eventProcess.value = Event(InProcess(SearchPlacesProcess)) }
                .onEach { notifyEventProcessToFinish(it, SearchPlacesProcess) }
                .catch {
                    notifyCommonExceptions(it, SearchPlacesProcess)
                }
                .asLiveData()
        }

    fun clearResult() {
        _clearResult.value = Event(1)
    }

    fun setPosition(latitude: Double, longitude: Double) {
        viewModelScope.launch {
            val locationGrid = createLocationGridOutOfDistance(latitude, longitude)
            currentPositionChannel.value = locationGrid
        }
    }

    val nearbyRestaurants: LiveData<Event<DataResult<List<Restaurant>>>> =
        currentPositionChannel.switchMap { currentPosition ->
            repository.retrieveNearbyPlaces(
                currentPosition,
                RADIUS_METER_SEARCH,
                TYPE_RESTAURANT
            )
                .onStart { _eventProcess.value = Event(InProcess(LoadNearbyPlacesProcess)) }
                .onEach { notifyEventProcessToFinish(it, LoadNearbyPlacesProcess) }
                .catch { notifyCommonExceptions(it, LoadNearbyPlacesProcess) }
                .asLiveData()
        }

    val nearbyCafes: LiveData<Event<DataResult<List<Restaurant>>>> =
        currentPositionChannel.switchMap { currentPosition ->
            repository.retrieveNearbyPlaces(
                currentPosition,
                RADIUS_METER_SEARCH,
                TYPE_CAFE
            )
                .onStart { _eventProcess.value = Event(InProcess(LoadNearbyPlacesProcess)) }
                .onEach { notifyEventProcessToFinish(it, LoadNearbyPlacesProcess) }
                .catch {
                    notifyCommonExceptions(it, LoadNearbyPlacesProcess)
                }
                .asLiveData()
        }

    val nearbyDelivery: LiveData<Event<DataResult<List<Restaurant>>>> =
        currentPositionChannel.switchMap { currentPosition ->
            repository.retrieveNearbyPlaces(
                currentPosition,
                RADIUS_METER_SEARCH,
                TYPE_DELIVERY
            )
                .onStart { _eventProcess.value = Event(InProcess(LoadNearbyPlacesProcess)) }
                .onEach { notifyEventProcessToFinish(it, LoadNearbyPlacesProcess) }
                .catch { notifyCommonExceptions(it, LoadNearbyPlacesProcess) }
                .asLiveData()
        }

    private fun notifyEventProcessToFinish(
        it: Event<DataResult<List<Restaurant>>>,
        eventProcessKey: EventProcessKey,
    ) {
        if (!it.isConsumed) {
            //should not consume in a viewModel, only in uia logic
            val dataResult = it.peek()
            if (dataResult is DataResult.Success) {
                _eventProcess.value = Event(Finish(eventProcessKey))
            } else if (dataResult is DataResult.Error) {
                _eventProcess.value = Event(Error(eventProcessKey, dataResult.exception))
            }
        }
    }

    fun translate(searchActionState: SearchViewState): String = when (searchActionState) {
        SearchViewState.NearbyCafes -> "Nearby Cafes"
        SearchViewState.SearchTextResult -> "Search Result"
        SearchViewState.NearbyRestaurants -> "Nearby Restaurants"
        SearchViewState.NearbyDelivery -> "Nearby Delivery"
        SearchViewState.QuickActionNearbyOptions -> ""
    }
}