package com.devofure.ambrosiajournal.presenter.ui.menuItem

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.os.Environment
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.core.view.doOnPreDraw
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.databinding.FragmentMenuItemBinding
import com.devofure.ambrosiajournal.domain.DataResult
import com.devofure.ambrosiajournal.domain.MenuItem
import com.devofure.ambrosiajournal.domain.Restaurant
import com.devofure.ambrosiajournal.domain.UserReviewNote
import com.devofure.ambrosiajournal.presenter.NavigationDestination
import com.devofure.ambrosiajournal.presenter.NavigationViewModel
import com.devofure.ambrosiajournal.presenter.adapter.MenuItemNoteListAdapter
import com.devofure.ambrosiajournal.presenter.adapter.PhotoItemListAdapter
import com.devofure.ambrosiajournal.presenter.navigateNowTo
import com.devofure.ambrosiajournal.presenter.ui.place.PlaceFragment
import com.devofure.ambrosiajournal.presenter.utils.connectWithNavController
import com.devofure.ambrosiajournal.presenter.utils.themeColor
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.transition.MaterialContainerTransform
import dagger.hilt.android.AndroidEntryPoint
import java.io.File


@AndroidEntryPoint
class MenuItemFragment : Fragment() {

    private val args: MenuItemFragmentArgs by navArgs()
    private val menuItemViewModel: MenuItemViewModel by viewModels()
    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.mobile_navigation)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        menuItemViewModel.setMenuItemId(args.menuItemKey)

        sharedElementEnterTransition = MaterialContainerTransform().apply {
            // Scope the transition to a view in the hierarchy so we know it will be added under
            // the bottom app bar but over the elevation scale of the exiting HomeFragment.
            drawingViewId = R.id.nav_host_fragment
            duration = resources.getInteger(R.integer.motion_duration_extra_large).toLong()
            scrimColor = Color.TRANSPARENT
            setAllContainerColors(requireContext().themeColor(R.attr.colorSurface))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val binding: FragmentMenuItemBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_menu_item,
            container,
            false
        )
        val userReviewNoteAdapterList = userReviewNoteItemListAdapter()
        val photoAdapterList = photoItemListAdapter()
        bindViews(binding, userReviewNoteAdapterList, photoAdapterList)
        observeMenuItem(binding)
        observeReviewNoteList(userReviewNoteAdapterList)
        observePhotos(photoAdapterList)
        setupNavigation()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Postpone enter transitions to allow shared element transitions to run.
        // https://github.com/googlesamples/android-architecture-components/issues/495
        postponeEnterTransition()
        view.doOnPreDraw { startPostponedEnterTransition() }
    }

    private fun bindViews(
        binding: FragmentMenuItemBinding,
        userReviewNoteAdapterList: MenuItemNoteListAdapter,
        photoAdapterList: PhotoItemListAdapter,
    ) {
        binding.navigationViewModel = navigationViewModel
        binding.viewModel = menuItemViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        binding.reviewNotes.adapter = userReviewNoteAdapterList
        binding.photosList.adapter = photoAdapterList
        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbar)
        binding.toolbar.connectWithNavController(findNavController())
    }

    private fun observeMenuItem(binding: FragmentMenuItemBinding) {
        menuItemViewModel.menuItem.observe(viewLifecycleOwner, {
            if (it is DataResult.Success) {
                binding.viewModelItem = it.data
            } else if (it is DataResult.Error) {
                it.exception.printStackTrace()
            }
        })
    }

    private fun observeReviewNoteList(userReviewNoteAdapterList: MenuItemNoteListAdapter) {
        menuItemViewModel.userReviewNoteList.observe(viewLifecycleOwner, {
            if (it is DataResult.Success) {
                userReviewNoteAdapterList.submitList(it.data)
            } else if (it is DataResult.Error) {
                it.exception.printStackTrace()
            }
        })
    }

    private fun observePhotos(photoAdapterList: PhotoItemListAdapter) =
        menuItemViewModel.menuItemPhotos.observe(viewLifecycleOwner, {
            if (it is DataResult.Success) {
                photoAdapterList.submitList(it.data)
            }
        })

    private fun userReviewNoteItemListAdapter() =
        MenuItemNoteListAdapter(object : MenuItemNoteListAdapter.MenuItemNoteAdapterListener {
            override fun onMenuDeleteClicked(note: UserReviewNote) =
                menuItemViewModel.deleteReviewNote(note)

            override fun onMenuEditClicked(note: UserReviewNote): Boolean {
                navigationViewModel.navigateToEditNote(note)
                return true
            }
        })

    private fun setupNavigation() {
        navigationViewModel.newDestination.observe(viewLifecycleOwner, Observer { event ->
            when (val eventType = event.peek()) {
                is NavigationDestination.NavigationRate<*> -> {
                    if (event.consume() == null) return@Observer
                    if (eventType.item is MenuItem && eventType.item.id != null) {
                        findNavController().navigate(
                            MenuItemFragmentDirections.actionMenuItemFragmentToRateMenuItemDialogFragment(
                                eventType.item.id!!
                            )
                        )
                    }
                }
                is NavigationDestination.Details<*> -> {
                    if (eventType.item is Restaurant) {
                        if (event.consume() == null) return@Observer
                        navigateNowTo(PlaceFragment::class.java,
                            PlaceFragment.generateBundle(eventType.item.id))
                    }
                }
                is NavigationDestination.AddReviewNote<*> -> {
                    if (eventType.item is MenuItem) {
                        if (event.consume() == null) return@Observer
                        if (eventType.item.id == null) return@Observer
                        findNavController().navigate(
                            MenuItemFragmentDirections
                                .actionMenuItemFragmentToAddEditReviewNoteDialogFragment(
                                    menuItemKey = eventType.item.id!!,
                                    reviewNoteKey = null
                                )
                        )
                    }
                }
                is NavigationDestination.EditReviewNote<*> -> {
                    if (eventType.item is UserReviewNote) {
                        if (event.consume() == null) return@Observer
                        findNavController().navigate(
                            MenuItemFragmentDirections
                                .actionMenuItemFragmentToAddEditReviewNoteDialogFragment(
                                    menuItemKey = eventType.item.menuItemId,
                                    reviewNoteKey = eventType.item.id
                                )
                        )
                    }
                }
                is NavigationDestination.NavigationAddPhoto<*> -> {
                    if (eventType.item is MenuItem) {
                        if (event.consume() == null) return@Observer
                        retrieveImage()
                    }
                }
            }
        })
    }

    private fun retrieveImage() {
        ImagePicker.with(this)
            .galleryMimeTypes(  //Exclude gif images
                mimeTypes = arrayOf(
                    "image/png",
                    "image/jpg",
                    "image/jpeg"
                )
            )
            .crop()
            .compress(1024)
            .maxResultSize(1024, 1024)
            .saveDir(File(Environment.getExternalStorageDirectory(),
                getString(R.string.app_name)))
            .start { resultCode, intent ->
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        //Image Uri will not be null for RESULT_OK
                        val file: File = ImagePicker.getFile(intent)!!
                        val filePath: String = ImagePicker.getFilePath(intent)!!
                        menuItemViewModel.addImage(args.menuItemKey,
                            filePath,
                            file.name,
                            null,
                            1024,
                            1024)
                    }
                    ImagePicker.RESULT_ERROR -> {
                        Toast.makeText(context,
                            ImagePicker.getError(intent),
                            Toast.LENGTH_SHORT).show()
                    }
                }
            }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) =
        inflater.inflate(R.menu.menu_item_menu, menu)

    override fun onOptionsItemSelected(item: android.view.MenuItem): Boolean =
        when (item.itemId) {
            R.id.editId -> {
                val menuItemResultData = menuItemViewModel.menuItem.value
                if (menuItemResultData is DataResult.Success<MenuItem>)
                    findNavController().navigate(
                        MenuItemFragmentDirections.actionMenuItemFragmentToAddMenuItemDialogFragment(
                            menuItemResultData.data.id, menuItemResultData.data.restaurantId
                        )
                    )
                true
            }
            R.id.deleteId -> {
                menuItemViewModel.delete()
                findNavController().navigateUp()
            }
            else -> super.onOptionsItemSelected(item)
        }

    private fun photoItemListAdapter() = PhotoItemListAdapter()

    companion object {
        fun generateBundle(menuItemKey: String) = bundleOf("menuItemKey" to menuItemKey)
    }
}