package com.devofure.ambrosiajournal.presenter.ui.place

import android.graphics.Color
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.core.view.doOnPreDraw
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.data.NoInternetAvailable
import com.devofure.ambrosiajournal.databinding.FragmentPlaceBinding
import com.devofure.ambrosiajournal.domain.DataResult
import com.devofure.ambrosiajournal.domain.MenuItem
import com.devofure.ambrosiajournal.domain.Restaurant
import com.devofure.ambrosiajournal.presenter.NavigationDestination
import com.devofure.ambrosiajournal.presenter.NavigationViewModel
import com.devofure.ambrosiajournal.presenter.adapter.MenuItemListAdapter
import com.devofure.ambrosiajournal.presenter.adapter.PhotoItemListAdapter
import com.devofure.ambrosiajournal.presenter.base.BaseFragment
import com.devofure.ambrosiajournal.presenter.base.EventProcess
import com.devofure.ambrosiajournal.presenter.navigateNowToMenuItemDetailWithTransition
import com.devofure.ambrosiajournal.presenter.ui.menuItem.MenuItemViewModel
import com.devofure.ambrosiajournal.presenter.utils.*
import com.google.android.material.transition.MaterialContainerTransform
import com.google.android.material.transition.MaterialElevationScale
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PlaceFragment : BaseFragment() {

    private lateinit var menuItemAdapterList: MenuItemListAdapter
    private val args: PlaceFragmentArgs by navArgs()
    private val restaurantViewModel: RestaurantViewModel by viewModels()
    private val menuItemViewModel: MenuItemViewModel by viewModels()
    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.mobile_navigation)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        restaurantViewModel.notifyRestaurantIdWithSync(args.restaurantKey)

        sharedElementEnterTransition = MaterialContainerTransform().apply {
            // Scope the transition to a view in the hierarchy so we know it will be added under
            // the bottom app bar but over the elevation scale of the exiting HomeFragment.
            drawingViewId = R.id.nav_host_fragment
            duration = resources.getInteger(R.integer.motion_duration_extra_large).toLong()
            scrimColor = Color.TRANSPARENT
            setAllContainerColors(requireContext().themeColor(R.attr.colorSurface))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val binding: FragmentPlaceBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_place,
            container,
            false
        )
        //TODO for test right now
        //binding.swipeRefreshLayout.isEnabled = false

        menuItemAdapterList = menuItemListAdapter(binding)
        val photoAdapterList = PhotoItemListAdapter()
        bindViews(binding, menuItemAdapterList, photoAdapterList)
        observeRestaurant(binding)
        observeMenuList(menuItemAdapterList)
        observePhotos(photoAdapterList)
        setupNavigation()
        setupEventProcess(binding)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Postpone enter transitions to allow shared element transitions to run.
        // https://github.com/googlesamples/android-architecture-components/issues/495
        postponeEnterTransition()
        view.doOnPreDraw { startPostponedEnterTransition() }
    }

    private fun menuItemListAdapter(binding: FragmentPlaceBinding): MenuItemListAdapter {
        return MenuItemListAdapter(object : MenuItemListAdapter.MenuItemAdapterListener {
            override fun onClicked(cardView: View, menuItem: MenuItem) {
                setupTransitionToDetailFragment(binding.fab)
                cardView.navigateNowToMenuItemDetailWithTransition(menuItem, binding.fab)
            }

            override fun onStarChanged(menuItem: MenuItem, newValue: Boolean, position: Int) {
                menuItemViewModel.switchBookmark(menuItem)
                menuItemAdapterList.notifyItemChanged(position)
            }
        })
    }

    private fun setupTransitionToDetailFragment(fab: Any) {
        val motionDuration = resources.getInteger(R.integer.motion_duration_extra_large).toLong()
        exitTransition = MaterialElevationScale(false).apply { duration = motionDuration }
        reenterTransition = MaterialElevationScale(true).apply { duration = motionDuration }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) =
        inflater.inflate(R.menu.place_menu, menu)

    override fun onOptionsItemSelected(item: android.view.MenuItem): Boolean =
        when (item.itemId) {
            R.id.refresh -> {
                restaurantViewModel.notifyRestaurantIdWithSync(args.restaurantKey, true)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    private fun bindViews(
        binding: FragmentPlaceBinding,
        menuAdapterList: MenuItemListAdapter,
        photoAdapterList: PhotoItemListAdapter,
    ) {
        binding.navigationViewModel = navigationViewModel
        binding.viewModel = restaurantViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbar)
        binding.toolbar.connectWithNavController(findNavController())
        binding.menuList.adapter = menuAdapterList
        binding.photosList.adapter = photoAdapterList
    }

    private fun observeRestaurant(binding: FragmentPlaceBinding) =
        restaurantViewModel.restaurant.observe(viewLifecycleOwner, {
            if (it is DataResult.Success) {
                binding.viewModelItem = it.data
            } else if (it is DataResult.Error) {
                if (it.exception is NoInternetAvailable) {
                    showSimpleMessage(R.string.error_no_internet_available)
                }
            }
        })

    private fun observePhotos(photoAdapterList: PhotoItemListAdapter) =
        restaurantViewModel.restaurantPhotos.observe(viewLifecycleOwner, {
            if (it is DataResult.Success) {
                photoAdapterList.submitList(it.data)
            }
        })

    private fun observeMenuList(menuAdapterList: MenuItemListAdapter) =
        restaurantViewModel.restaurantMenu.observe(viewLifecycleOwner, {
            if (it is DataResult.Success) {
                menuAdapterList.submitList(it.data)
            } else if (it is DataResult.Error) {
                it.exception.printStackTrace()
            }
        })

    private fun setupNavigation() {
        navigationViewModel.newDestination.observe(viewLifecycleOwner, Observer { event ->
            when (val eventType = event.peek()) {
                is NavigationDestination.AddMenuItem<*> -> {
                    if (event.consume() == null) return@Observer
                    val item = eventType.item as Restaurant
                    findNavController().navigate(
                        PlaceFragmentDirections.restaurantDetailsToAddMenuItemDialog(null,
                            item.id)
                    )
                }
                is NavigationDestination.NavigationRate<*> -> {
                    if (event.consume() == null) return@Observer
                    val item = eventType.item as Restaurant
                    findNavController().navigate(
                        PlaceFragmentDirections.actionRestaurantFragmentToRateRestaurantDialogFragment(
                            item.id
                        )
                    )
                }
                is NavigationDestination.Website -> {
                    if (event.consume() == null) return@Observer
                    goToLink(requireContext(), eventType.url)
                }
                is NavigationDestination.PhoneCall -> {
                    if (event.consume() == null) return@Observer
                    goToPhoneToCall(requireContext(), eventType.phoneNumber)
                }
                else -> {
                }
            }
        })
    }

    private fun setupEventProcess(binding: FragmentPlaceBinding) {
        restaurantViewModel.eventProcess.observe(viewLifecycleOwner, {
            when (val eventProcess = it.peek()) {
                is EventProcess.Error -> handleException(it)
            }
        })
    }

    companion object {
        fun generateBundle(placeId: String) = bundleOf("restaurantKey" to placeId)
    }
}
