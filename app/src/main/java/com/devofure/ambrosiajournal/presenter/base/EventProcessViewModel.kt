package com.devofure.ambrosiajournal.presenter.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.crashlytics.FirebaseCrashlytics

/**
 * Event process state , K is the key of the state
 */
sealed class EventProcess<PK> {
    data class InProcess<PK>(val eventKey: PK) : EventProcess<PK>()
    data class Finish<PK>(val eventKey: PK) : EventProcess<PK>()
    data class Error<PK>(val eventKey: PK, val exception: Throwable) : EventProcess<PK>()
}

/**
 * Takes in _eventProcess that is MutableLiveData with a event state type that also defines the key of the state
 */
open class EventProcessViewModel<PK, RK> constructor(
    protected val _eventProcess: MutableLiveData<Event<EventProcess<PK>>>,
    @Suppress("MemberVisibilityCanBePrivate")
    protected val _eventRequest: MutableLiveData<Event<RK>>,
) : ViewModel() {

    /**
     * a liveData that tells
     */
    internal val eventProcess: LiveData<Event<EventProcess<PK>>> get() = _eventProcess
    internal val eventRequest: LiveData<Event<RK>> get() = _eventRequest

    /**
     * wraps an event action in a process state. tells when the process starts, finish or fail.
     */
    suspend fun runEventProcess(
        eventKey: PK,
        actionEvent: suspend () -> Unit,
        handleExceptionEvent: (suspend (Throwable) -> Unit)? = null,
    ): suspend () -> Unit {
        _eventProcess.value = Event(EventProcess.InProcess(eventKey))
        try {
            actionEvent()
        } catch (exception: Throwable) {
            FirebaseCrashlytics.getInstance().log(eventKey.toString())
            FirebaseCrashlytics.getInstance().recordException(exception)
            exception.printStackTrace()
            if (handleExceptionEvent != null) {
                handleExceptionEvent(exception)
            } else {
                _eventProcess.value = Event(EventProcess.Error(
                    eventKey,
                    exception
                ))
            }
            return actionEvent
        }
        _eventProcess.value =
            Event(EventProcess.Finish(eventKey))
        return actionEvent
    }
}