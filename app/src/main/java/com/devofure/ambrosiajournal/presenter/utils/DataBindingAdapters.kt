package com.devofure.ambrosiajournal.presenter.utils

import android.graphics.Typeface
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.bumptech.glide.Glide
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.domain.Photo
import com.google.android.material.slider.Slider
import java.io.File

@BindingAdapter("photoLocal")
fun setImagePath(imageView: ImageView, mainPhoto: Photo) {
    if (mainPhoto.localPah != null) {
        Glide.with(imageView.context)
            .load(File(mainPhoto.localPah))
            .into(imageView)
    } else {
        mainPhoto.photoReference.run {
            val placesApiKey = imageView.context.getString(R.string.google_places_api_key)
            Glide.with(imageView.context)
                .load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$this&key=$placesApiKey")
                .into(imageView)
        }
    }
}

@BindingAdapter("simplePhotoLocal")
fun setImagePath(imageView: ImageView, localPah: String?) {
    Glide.with(imageView.context)
        .load(localPah?.let { File(it) })
        .fallback(R.drawable.ic_baseline_restaurant_24)
        .into(imageView)
}

@BindingAdapter("photoUrls")
fun setImageUrl(imageView: ImageView, mainPhoto: String?) {
    mainPhoto?.run {
        val placesApiKey = imageView.context.getString(R.string.google_places_api_key)
        Glide.with(imageView.context)
            .load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=$this&key=$placesApiKey")
            .into(imageView)
    }
}

@InverseBindingAdapter(attribute = "android:value")
fun getSliderValue(slider: Slider) = slider.value

@BindingAdapter("android:valueAttrChanged")
fun setSliderListeners(slider: Slider, attrChange: InverseBindingListener) {
    slider.addOnChangeListener { _, _, _ ->
        attrChange.onChange()
    }
}

@BindingAdapter("bindTextStyle")
fun setTextStyle(textView: TextView, newTextStyleValue: Int) {
    if (newTextStyleValue == Typeface.BOLD) {
        textView.setTypeface(null, Typeface.BOLD)
    } else {
        textView.setTypeface(null, Typeface.NORMAL)
    }
}
