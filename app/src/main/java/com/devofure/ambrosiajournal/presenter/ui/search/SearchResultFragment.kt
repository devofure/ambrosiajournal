package com.devofure.ambrosiajournal.presenter.ui.search

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.doOnPreDraw
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.databinding.FragmentSearchResulttBinding
import com.devofure.ambrosiajournal.domain.DataResult
import com.devofure.ambrosiajournal.domain.Restaurant
import com.devofure.ambrosiajournal.presenter.NavigationViewModel
import com.devofure.ambrosiajournal.presenter.adapter.PlaceItemListAdapter
import com.devofure.ambrosiajournal.presenter.base.BaseFragment
import com.devofure.ambrosiajournal.presenter.base.Event
import com.devofure.ambrosiajournal.presenter.base.EventProcess
import com.devofure.ambrosiajournal.presenter.base.EventRequestKey
import com.devofure.ambrosiajournal.presenter.navigateNowToPlaceDetailsWithTransition
import com.devofure.ambrosiajournal.presenter.ui.place.RestaurantViewModel
import com.devofure.ambrosiajournal.presenter.ui.search.SearchViewState.*
import com.devofure.ambrosiajournal.presenter.utils.*
import com.google.android.material.transition.MaterialContainerTransform
import com.google.android.material.transition.MaterialElevationScale
import com.google.android.material.transition.MaterialFadeThrough
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchResultFragment : BaseFragment() {

    private lateinit var adapterList: PlaceItemListAdapter
    private val searchViewModel: SearchViewModel by activityViewModels()
    private val placeViewModel: RestaurantViewModel by activityViewModels()
    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.mobile_navigation)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enterTransition = MaterialFadeThrough().apply {
            duration = resources.getInteger(R.integer.motion_duration_medium).toLong()
        }
        sharedElementEnterTransition = MaterialContainerTransform().apply {
            // Scope the transition to a view in the hierarchy so we know it will be added under
            // the bottom app bar but over the elevation scale of the exiting HomeFragment.
            drawingViewId = R.id.nav_host_fragment
            duration = resources.getInteger(R.integer.motion_duration_large).toLong()
            scrimColor = Color.TRANSPARENT
            setAllContainerColors(requireContext().themeColor(R.attr.colorSurface))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val binding: FragmentSearchResulttBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_search_resultt,
            container,
            false
        )
        //TODO for test right now
        binding.swipeRefreshLayout.isEnabled = false

        adapterList = placeItemListAdapter()
        binding.list.adapter = adapterList
        bindViews(binding)
        setupSearchResultState(binding, adapterList)
        setupLoadingView(binding)
        observeEventRequest(adapterList)
        setupEventProcess()
        return binding.root
    }

    private fun placeItemListAdapter(): PlaceItemListAdapter {
        return PlaceItemListAdapter(true, placeAdapterListener())
    }

    private fun placeAdapterListener(): PlaceItemListAdapter.PlaceAdapterListener =
        object : PlaceItemListAdapter.PlaceAdapterListener {
            override fun onClicked(cardView: View, place: Restaurant) {
                setupTransitionToDetailFragment()
                cardView.navigateNowToPlaceDetailsWithTransition(place)
            }

            override fun onStarChanged(place: Restaurant, newValue: Boolean, position: Int) {
                placeViewModel.switchBookmark(place)
                adapterList.notifyItemChanged(position)
            }
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Postpone enter transitions to allow shared element transitions to run.
        // https://github.com/googlesamples/android-architecture-components/issues/495
        postponeEnterTransition()
        view.doOnPreDraw { startPostponedEnterTransition() }
    }

    private fun setupTransitionToDetailFragment() {
        val motionDuration = resources.getInteger(R.integer.motion_duration_extra_large).toLong()
        exitTransition = MaterialElevationScale(false).apply { duration = motionDuration }
        reenterTransition = MaterialElevationScale(true).apply { duration = motionDuration }
    }

    private fun observeEventRequest(adapterList: PlaceItemListAdapter) {
        placeViewModel.eventRequest.observe(viewLifecycleOwner, Observer {
            when (val eventRequestKey = it.peek()) {
                is EventRequestKey.UpdateItemInList -> {
                    if (it.consume() == null) return@Observer
                    adapterList.notifyItemChanged(eventRequestKey.currentPosition)
                }
            }
        })
    }

    private fun setupLoadingView(
        binding: FragmentSearchResulttBinding,
    ) {
        searchViewModel.eventProcess.observe(viewLifecycleOwner, {
            when (it.peek()) {
                is EventProcess.InProcess -> {
                    binding.swipeRefreshLayout.isRefreshing = true
                    //binding.loadingView.show()
                }
                is EventProcess.Finish -> {
                    binding.swipeRefreshLayout.isRefreshing = false
                    //binding.loadingView.hide()
                }
                is EventProcess.Error -> {
                    binding.swipeRefreshLayout.isRefreshing = false
                    //binding.loadingView.hide()
                }
            }
        })
    }

    private fun bindViews(binding: FragmentSearchResulttBinding) {
        binding.activateClearButton = true
        binding.navigationViewModel = navigationViewModel
        (activity as AppCompatActivity?)!!.setSupportActionBar(binding.toolbar)
        binding.toolbar.connectWithNavController(findNavController())
    }

    private fun setupSearchResultState(
        binding: FragmentSearchResulttBinding,
        adapterList: PlaceItemListAdapter,
    ) {
        searchViewModel.clearResult.observe(viewLifecycleOwner, {
            adapterList.submitList(emptyList())
            binding.listTitle = ""
        })
        searchViewModel.searchResultState.observe(viewLifecycleOwner, {
            when (it.id) {
                SearchTextResult.id -> {
                    searchViewModel.placeTextSearchList.observe(viewLifecycleOwner, { event ->
                        populateListAdapter(event, adapterList, binding, SearchTextResult)
                        binding.toolbarTitle = searchViewModel.searchQueryText.value
                    })
                }
                NearbyRestaurants.id -> {
                    searchViewModel.nearbyRestaurants.observe(viewLifecycleOwner, { event ->
                        populateListAdapter(event, adapterList, binding, NearbyRestaurants)
                    })
                    binding.toolbarTitle = "Nearby Restaurants"
                }
                NearbyCafes.id -> {
                    searchViewModel.nearbyCafes.observe(viewLifecycleOwner, { event ->
                        populateListAdapter(event, adapterList, binding, NearbyCafes)
                    })
                    binding.toolbarTitle = "Nearby Cafes"
                }
                NearbyDelivery.id -> {
                    searchViewModel.nearbyDelivery.observe(viewLifecycleOwner, { event ->
                        populateListAdapter(event, adapterList, binding, NearbyDelivery)
                    })
                    binding.toolbarTitle = "Nearby Delivery Places"
                }
            }
        })
    }

    private fun populateListAdapter(
        event: Event<DataResult<List<Restaurant>>>,
        adapterList: PlaceItemListAdapter,
        binding: FragmentSearchResulttBinding,
        searchActionState: SearchViewState,
    ) {
        val eventData = event.peek()
        if (eventData is DataResult.Success) {
            //Always consume to notify event process that this data has been read
            event.consume()
            adapterList.submitList(eventData.data)
            setResultTitle(binding, searchActionState, eventData.data.size)
        } else {
            adapterList.submitList(emptyList())
            setResultTitle(binding, searchActionState, 0)
        }
    }

    private fun setResultTitle(
        binding: FragmentSearchResulttBinding,
        searchViewState: SearchViewState,
        size: Int,
    ) {
        val searchActionStateText = searchViewModel.translate(searchViewState)
        binding.listTitle = "$searchActionStateText: $size"
    }

    private fun setupEventProcess() {
        searchViewModel.eventProcess.observe(viewLifecycleOwner, this::handleException)
    }
}