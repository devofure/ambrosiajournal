package com.devofure.ambrosiajournal.presenter.ui.search

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.navGraphViewModels
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.databinding.FragmentSearchBinding
import com.devofure.ambrosiajournal.hardware.LocationRetriever
import com.devofure.ambrosiajournal.presenter.NavigationDestination.SearchNavigation
import com.devofure.ambrosiajournal.presenter.NavigationViewModel
import com.devofure.ambrosiajournal.presenter.base.BaseFragment
import com.devofure.ambrosiajournal.presenter.base.withPermissions
import com.devofure.ambrosiajournal.presenter.navigateNowTo
import com.devofure.ambrosiajournal.presenter.ui.search.SearchViewState.*
import com.google.android.material.transition.MaterialElevationScale
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.vmadalin.easypermissions.annotations.AfterPermissionGranted
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

sealed class SearchViewState(val id: Int) {
    object QuickActionNearbyOptions : SearchViewState(0)
    object SearchTextResult : SearchViewState(1)
    object NearbyRestaurants : SearchViewState(2)
    object NearbyCafes : SearchViewState(3)
    object NearbyDelivery : SearchViewState(4)
}

@AndroidEntryPoint
class SearchFragment : BaseFragment() {

    companion object {
        private const val REQUEST_CODE_LOCATION: Int = 253154
    }

    private lateinit var mainContainer: ViewGroup
    private lateinit var searchView: SearchView
    private val searchViewModel: SearchViewModel by activityViewModels()
    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.mobile_navigation)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val binding: FragmentSearchBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_search,
            container,
            false
        )
        mainContainer = binding.mainContainer
        bindViews(binding)
        setupNavigation(binding.root)
        setupEventProcess()
        return binding.root
    }

    private fun setupNavigation(root: View) {
        navigationViewModel.newDestination.observe(viewLifecycleOwner, Observer {
            val navigationDestination = it.peek()

            if (navigationDestination !is SearchNavigation<*>) return@Observer

            if (isNearbyFoodPlace(navigationDestination)) {
                if (it.consume() == null) return@Observer
                showNearbySearchListWithPermission(navigationDestination.searchViewState, root)
            } else if (navigationDestination.searchViewState is QuickActionNearbyOptions) {
                if (it.consume() == null) return@Observer
                searchViewModel.clearResult()
                showSearchStatus(QuickActionNearbyOptions, root)
            }
        })
    }

    private fun isNearbyFoodPlace(navigationDestination: SearchNavigation<*>) =
        (navigationDestination.searchViewState is NearbyRestaurants
                || navigationDestination.searchViewState is NearbyCafes
                || navigationDestination.searchViewState is NearbyDelivery)

    @AfterPermissionGranted(REQUEST_CODE_LOCATION)
    private fun showNearbySearchListWithPermission(searchViewState: SearchViewState, root: View) =
        withPermissions(ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION)
            .runIfPermitted { showNearbySearchList(searchViewState, root) }
            .orAskPermissionIfNotGranted(
                rationale = "The app need permission to know the location to fetch places near you",
                requestCode = REQUEST_CODE_LOCATION,
            )
            .execute()

    private fun setupEventProcess() {
        searchViewModel.eventProcess.observe(viewLifecycleOwner, this::handleException)
    }

    private fun bindViews(binding: FragmentSearchBinding) {
        binding.viewModel = searchViewModel
        binding.navigationViewModel = navigationViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        searchView = binding.restaurantSearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query == null) {
                    showSearchStatus(QuickActionNearbyOptions, binding.root)
                } else {
                    searchViewModel.findRestaurantByName(query)
                    showSearchStatus(SearchTextResult, binding.root)
                }
                searchView.clearFocus()
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean = true
        })
    }

    private fun showNearbySearchList(searchViewState: SearchViewState, root: View) {
        viewLifecycleOwner.lifecycleScope.launch {
            val location = LocationRetriever(requireContext()).getLocation()
            location?.let {
                searchViewModel.setPosition(location.latitude, location.longitude)
                showSearchStatus(searchViewState, root)
            }
        }
    }

    private fun handleLocationError(error: Throwable) {
        FirebaseCrashlytics.getInstance().recordException(error)
        //Locus library handles already the permission
    }

    private fun showSearchStatus(searchViewState: SearchViewState, root: View) {
        searchViewModel.notifySearchViewState(searchViewState)
        val searchViewStateView = getSourceViewForDestination(searchViewState, root)
        val transitionName = requireContext().getString(R.string.search_result_transition_name)
        setupTransition()
        searchViewStateView.navigateNowTo(
            SearchFragmentDirections.actionNavigationSearchToNavigationSearchResultFragment(),
            transitionName
        )
    }

    private fun setupTransition() {
        val motionDuration = resources.getInteger(R.integer.motion_duration_extra_large).toLong()
        exitTransition = MaterialElevationScale(false).apply { duration = motionDuration }
        reenterTransition = MaterialElevationScale(true).apply { duration = motionDuration }
    }

    private fun getSourceViewForDestination(searchViewState: SearchViewState, root: View): View =
        when (searchViewState) {
            NearbyCafes -> root.findViewById(R.id.quickActionNearbyCafe)
            SearchTextResult -> searchView
            NearbyRestaurants -> root.findViewById(R.id.quickNearByRestaurant)
            NearbyDelivery -> root.findViewById(R.id.quickActionNearbyDelivery)
            else -> TODO() // should never happen
        }
}