package com.devofure.ambrosiajournal.presenter.base

import android.Manifest
import androidx.annotation.Size
import androidx.fragment.app.Fragment
import com.vmadalin.easypermissions.EasyPermissions

class PermissionBuilder(
    @Size(min = 1) private vararg val permissions: String,
    private val host: Fragment,
) {
    private lateinit var functionToRunWithPermission: () -> Unit
    private var requestCode: Int = 0
    private lateinit var rationale: String

    fun runIfPermitted(functionToRunWithPermission: () -> Unit): PermissionBuilder {
        this.functionToRunWithPermission = functionToRunWithPermission
        return this
    }

    fun orAskPermissionIfNotGranted(rationale: String, requestCode: Int): PermissionBuilder {
        this.rationale = rationale
        this.requestCode = requestCode
        return this
    }

    fun execute() {
        if (EasyPermissions.hasPermissions(host.requireContext(), *permissions)) {
            functionToRunWithPermission.invoke()
        } else if (requestCode != 0) {
            EasyPermissions.requestPermissions(
                host = host,
                rationale = "Location permission is required to fetch places nearby you",
                requestCode = requestCode,
                perms = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION)
            )
        }
    }
}