package com.devofure.ambrosiajournal.presenter.adapter

import android.view.View
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.databinding.ItemMenuItemBinding
import com.devofure.ambrosiajournal.domain.MenuItem

class MenuItemListAdapter(val listener: MenuItemAdapterListener) :
    ItemListAdapter<MenuItem, ItemMenuItemBinding>(R.layout.item_menu_item) {

    interface MenuItemAdapterListener {
        fun onClicked(cardView: View, menuItem: MenuItem)
        fun onStarChanged(menuItem: MenuItem, newValue: Boolean, position: Int)
    }

    override fun onBindItemCreated(itemViewHolder: ItemViewHolder) {
        itemViewHolder.binding.itemListener = listener
    }

    override fun onBindItem(
        itemBinding: ItemMenuItemBinding,
        eventData: BindingEventData<MenuItem>,
    ) {
        itemBinding.viewModelItem = eventData.item
        itemBinding.currentPosition = eventData.position
    }
}