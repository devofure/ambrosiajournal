package com.devofure.ambrosiajournal.presenter.ui.menuItem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.navigation.navGraphViewModels
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.data.local.mapToDomain
import com.devofure.ambrosiajournal.data.local.mapToEntity
import com.devofure.ambrosiajournal.databinding.DialogFragmentAddMenuItemBinding
import com.devofure.ambrosiajournal.domain.DataResult
import com.devofure.ambrosiajournal.presenter.NavigationDestination
import com.devofure.ambrosiajournal.presenter.NavigationViewModel
import com.devofure.ambrosiajournal.presenter.base.EventProcess
import com.devofure.ambrosiajournal.presenter.base.EventProcessKey
import com.devofure.ambrosiajournal.presenter.utils.hideKeyboard
import com.devofure.ambrosiajournal.presenter.utils.showSoftKeyboard
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class AddEditMenuItemDialogFragment : DialogFragment() {

    private val menuItemViewModel: MenuItemViewModel by viewModels()
    private val args: AddEditMenuItemDialogFragmentArgs by navArgs()
    private val navigationViewModel: NavigationViewModel by navGraphViewModels(R.id.mobile_navigation)
    override fun onStart() {
        super.onStart()
        dialog?.apply {
            this.window!!.setLayout(MATCH_PARENT, WRAP_CONTENT)
            this.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        val binding: DialogFragmentAddMenuItemBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_fragment_add_menu_item,
            container,
            false
        )
        bindViews(binding)
        setupEventProcess(binding)
        setupNavigation(binding)
        return binding.root
    }

    private fun setupEventProcess(binding: DialogFragmentAddMenuItemBinding) {
        menuItemViewModel.eventProcess.observe(viewLifecycleOwner, {
            when (val eventProcess = it.peek()) {
                is EventProcess.InProcess -> {
                    binding.loadingView.show()
                    if (eventProcess.eventKey is EventProcessKey.SaveMenuItemProcess && menuItemViewModel.saveMenuItemActionByUser) {
                        hideKeyboard(requireActivity(), binding.menuItemNameInput.editText!!)
                    }
                }
                is EventProcess.Finish -> {
                    binding.loadingView.hide()
                    if (eventProcess.eventKey is EventProcessKey.SaveMenuItemProcess && menuItemViewModel.saveMenuItemActionByUser) {
                        dialog?.cancel()
                    }
                }
                is EventProcess.Error -> {
                    binding.loadingView.hide()
                }
            }
        })
    }

    private fun bindViews(binding: DialogFragmentAddMenuItemBinding) {
        binding.viewModel = menuItemViewModel
        binding.navigationViewModel = navigationViewModel
        binding.lifecycleOwner = viewLifecycleOwner
        showSoftKeyboard(binding.menuItemNameInput.editText!!, requireActivity())
        if (args.menuItemKey != null) {
            menuItemViewModel.menuItem.observe(viewLifecycleOwner, {
                if (it is DataResult.Success) {
                    //Workaround to make a new copy
                    binding.menuItemViewModelItem = it.data.mapToEntity().mapToDomain()
                } else if (it is DataResult.Error) {
                    it.exception.printStackTrace()
                }
            })
            menuItemViewModel.setMenuItemId(args.menuItemKey!!)
        } else {
            binding.menuItemViewModelItem = menuItemViewModel.createTempMenuItem(args.restaurantKey)
        }
    }

    private fun setupNavigation(binding: DialogFragmentAddMenuItemBinding) {
        navigationViewModel.newDestination.observe(viewLifecycleOwner, Observer { event ->
            when (event.peek()) {
                is NavigationDestination.NavigationBack -> {
                    if (event.consume() == null) return@Observer
                    hideKeyboard(requireActivity(), binding.menuItemNameInput.editText!!)
                    dialog?.cancel()
                }
                else -> {
                }
            }
        })
    }
}