package com.devofure.ambrosiajournal.presenter

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.domain.MenuItem
import com.devofure.ambrosiajournal.domain.Restaurant
import com.devofure.ambrosiajournal.domain.UserReviewNote
import com.devofure.ambrosiajournal.presenter.NavigationDestination.*
import com.devofure.ambrosiajournal.presenter.adapter.DomainModel
import com.devofure.ambrosiajournal.presenter.base.Event
import com.devofure.ambrosiajournal.presenter.ui.menuItem.AddEditReviewNoteDialogFragment
import com.devofure.ambrosiajournal.presenter.ui.menuItem.MenuItemFragment
import com.devofure.ambrosiajournal.presenter.ui.place.PlaceFragment
import com.devofure.ambrosiajournal.presenter.ui.search.SearchViewState
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

sealed class NavigationDestination {
    object NavigationBack : NavigationDestination()
    data class SearchNavigation<I : SearchViewState>(val searchViewState: I) :
        NavigationDestination()

    data class EditReviewNote<I : DomainModel>(val item: I) : NavigationDestination()
    data class AddReviewNote<I : DomainModel>(val item: I) : NavigationDestination()
    data class NavigationRate<I : DomainModel>(val item: I) : NavigationDestination()
    data class NavigationAddPhoto<I : DomainModel>(val item: I) : NavigationDestination()
    data class AddMenuItem<I : DomainModel>(val item: I) : NavigationDestination()
    data class Details<I : DomainModel>(val item: I) : NavigationDestination()
    data class Website(val url: String) : NavigationDestination()
    data class PhoneCall(val phoneNumber: String) : NavigationDestination()
}

@HiltViewModel
class NavigationViewModel @Inject constructor(
) : ViewModel() {

    private val _newDestination: MutableLiveData<Event<NavigationDestination>> = MutableLiveData()
    val newDestination get() : LiveData<Event<NavigationDestination>> = _newDestination

    fun navigateBack() {
        _newDestination.value = Event(NavigationBack)
    }

    fun navigateToPhoneCall(phoneNumber: String) {
        _newDestination.value = Event(PhoneCall(phoneNumber))
    }

    fun navigateToWebsite(url: String) {
        _newDestination.value = Event(Website(url))
    }

    fun navigateToNearbyRestaurants() {
        _newDestination.value = Event(SearchNavigation(SearchViewState.NearbyRestaurants))
    }

    fun navigateToNearbyDelivery() {
        _newDestination.value = Event(SearchNavigation(SearchViewState.NearbyDelivery))
    }

    fun navigateToNearbyCafes() {
        _newDestination.value = Event(SearchNavigation(SearchViewState.NearbyCafes))
    }

    fun navigateToAddMenuItem(restaurant: Restaurant) {
        _newDestination.value = Event(AddMenuItem(restaurant))
    }

    fun navigateToAddNote(menuItem: MenuItem) {
        _newDestination.value = Event(AddReviewNote(menuItem))
    }

    fun navigateToEditNote(reviewNote: UserReviewNote) {
        _newDestination.value = Event(EditReviewNote(reviewNote))
    }

    fun <I : DomainModel> navigateToDetails(item: I) {
        _newDestination.value = Event(Details(item))
    }

    fun <I : DomainModel> navigateToRate(item: I) {
        _newDestination.value = Event(NavigationRate(item))
    }

    fun <I : DomainModel> navigateToAddPhoto(item: I) {
        _newDestination.value = Event(NavigationAddPhoto(item))
    }
}

fun View.navigateNowToMenuItemDetailWithTransition(
    menuItem: MenuItem,
    fab: ExtendedFloatingActionButton? = null,
) {
    val bundle = MenuItemFragment.generateBundle(menuItem.id!!)
    val menuItemDetailTransitionName =
        this.context.getString(R.string.menu_item_detail_transition_name)

    val listOfExtras = listOfNotNull(
        this to menuItemDetailTransitionName,
        //fab?.run { this to "menu_item_fab_transition_name" }
    )

    val extras = FragmentNavigatorExtras(*listOfExtras.toTypedArray())
    navigateNow(this.findNavController(), MenuItemFragment::class.java, bundle, extras)
}

fun View.navigateNowToPlaceDetailsWithTransition(place: Restaurant) {
    val bundle = PlaceFragment.generateBundle(place.id)
    val placeDetailTransitionName = this.context.getString(R.string.place_detail_transition_name)

    /*val imageTrans: Pair<View, String>? = place.mainPhoto?.let {
        val mainImageTransitionName = this.context.getString(R.string.image_transition_name, place.id)
        this.findViewById<View>(R.id.mainImageView) to mainImageTransitionName
    }*/

    val transitionNames: List<Pair<View, String>> = listOfNotNull(
        this to placeDetailTransitionName,
        //imageTrans
    )
    val extras = FragmentNavigatorExtras(*transitionNames.toTypedArray())
    navigateNow(this.findNavController(), PlaceFragment::class.java, bundle, extras)
}

fun View.navigateNowTo(navDirections: NavDirections, transitionName: String?) {
    if (transitionName != null) {
        val extras = FragmentNavigatorExtras(this to transitionName)
        this.findNavController().navigate(navDirections, extras)
    } else {
        this.findNavController().navigate(navDirections)
    }
}

fun <F> Fragment.navigateNowTo(fragmentDestination: Class<F>, bundle: Bundle) {
    navigateNow(this.findNavController(), fragmentDestination, bundle, null)
}

private fun <F> navigateNow(
    nav: NavController,
    fragmentDestination: Class<F>,
    bundle: Bundle,
    extras: FragmentNavigator.Extras?,
) {
    when (fragmentDestination) {
        PlaceFragment::class.java ->
            nav.navigate(R.id.action_global_restaurant_fragment, bundle, null, extras)

        AddEditReviewNoteDialogFragment::class.java ->
            nav.navigate(R.id.action_menu_item_fragment_to_addEditReviewNoteDialogFragment,
                bundle,
                null,
                extras)

        MenuItemFragment::class.java ->
            nav.navigate(R.id.action_global_menu_item_fragment,
                bundle,
                null,
                extras)
    }
}