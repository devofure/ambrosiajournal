package com.devofure.ambrosiajournal.presenter.base

import android.widget.Toast
import androidx.annotation.Size
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.devofure.ambrosiajournal.BuildConfig
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.data.NoDataAvailable
import com.devofure.ambrosiajournal.data.NoInternetAvailable

open class BaseFragment : Fragment() {

    protected fun handleException(it: Event<EventProcess<EventProcessKey>>) {
        if (it.isConsumed) return
        val errorProcessEvent = it.peek()
        if (errorProcessEvent is EventProcess.Error<EventProcessKey>) {
            when (errorProcessEvent.exception) {
                is NoInternetAvailable -> {
                    it.consume()
                    showSimpleMessage(R.string.error_no_internet_available)
                }
                is NoDataAvailable -> {
                    it.consume()
                    showSimpleMessage(R.string.error_no_data_available)
                }
                else -> {
                    it.consume()
                    showSimpleMessage(errorProcessEvent.exception.message ?: "Unknown Error")
                }
            }
            if (BuildConfig.DEBUG) errorProcessEvent.exception.printStackTrace()
        }
    }

    protected fun showSimpleMessage(@StringRes stringResId: Int) {
        Toast.makeText(context,
            stringResId,
            Toast.LENGTH_LONG).show()
    }

    private fun showSimpleMessage(stringResId: String) {
        Toast.makeText(context,
            stringResId,
            Toast.LENGTH_LONG).show()
    }
}

fun Fragment.withPermissions(@Size(min = 1) vararg perms: String): PermissionBuilder {
    return PermissionBuilder(*perms, host = this, )
}