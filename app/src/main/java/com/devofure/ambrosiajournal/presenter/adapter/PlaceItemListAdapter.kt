package com.devofure.ambrosiajournal.presenter.adapter

import android.view.View
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.databinding.ItemPlaceBinding
import com.devofure.ambrosiajournal.domain.Restaurant

class PlaceItemListAdapter(
    private val showAllData: Boolean = false,
    val itemListener: PlaceAdapterListener,
) :
    ItemListAdapter<Restaurant, ItemPlaceBinding>(R.layout.item_place) {

    interface PlaceAdapterListener {
        fun onClicked(cardView: View, place: Restaurant)
        fun onStarChanged(place: Restaurant, newValue: Boolean, position: Int)
    }

    override fun onBindItemCreated(itemViewHolder: ItemViewHolder) {
        itemViewHolder.binding.itemListener = itemListener
        itemViewHolder.binding.showAllData = showAllData
    }

    override fun onBindItem(
        itemBinding: ItemPlaceBinding,
        eventData: BindingEventData<Restaurant>,
    ) {
        itemBinding.viewModelItem = eventData.item
        itemBinding.currentPosition = eventData.position
        //itemBinding.setVariable(BR.navigationViewModel, navigationViewModel)
    }

}