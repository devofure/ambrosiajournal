package com.devofure.ambrosiajournal.presenter

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.presenter.utils.goToLink
import com.devofure.ambrosiajournal.presenter.utils.sendEmail
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.vmadalin.easypermissions.EasyPermissions
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val navController = findNavController(R.id.nav_host_fragment)
        findViewById<BottomNavigationView>(R.id.nav_view).setupWithNavController(navController)
    }

    fun sendFeedback(view: View?) = sendEmail(
        this,
        getString(R.string.developer_email),
        getString(R.string.subject_feedback) + " " + getString(R.string.app_name),
        ""
    )

    fun toDeveloperPage(view: View) = goToLink(this, getString(R.string.linkedin))


    fun toPolicy(view: View) = goToLink(this, getString(R.string.project_policy))

    fun toTermsConditions(view: View) = goToLink(this, getString(R.string.project_terms_conditions))

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }
}