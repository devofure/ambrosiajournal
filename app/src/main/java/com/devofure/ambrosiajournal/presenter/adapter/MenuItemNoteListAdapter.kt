package com.devofure.ambrosiajournal.presenter.adapter

import android.view.Gravity
import androidx.appcompat.widget.PopupMenu
import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.databinding.ItemReviewNoteBinding
import com.devofure.ambrosiajournal.domain.UserReviewNote

class MenuItemNoteListAdapter(val listener: MenuItemNoteAdapterListener) :
    ItemListAdapter<UserReviewNote, ItemReviewNoteBinding>(R.layout.item_review_note) {

    interface MenuItemNoteAdapterListener {
        fun onMenuDeleteClicked(note: UserReviewNote)
        fun onMenuEditClicked(note: UserReviewNote): Boolean
    }

    override fun onBindItemCreated(itemViewHolder: ItemViewHolder) {
        itemViewHolder.binding.itemListener = listener
    }

    override fun onBindItem(
        itemBinding: ItemReviewNoteBinding,
        eventData: BindingEventData<UserReviewNote>,
    ) {
        itemBinding.viewModelItem = eventData.item
        itemBinding.moreMenuButton.setOnClickListener {
            val popup = PopupMenu(it.context, it, Gravity.BOTTOM)
            popup.inflate(R.menu.review_note_menu)
            popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { popupMenu ->
                when (popupMenu.itemId) {
                    R.id.delete -> {
                        listener.onMenuDeleteClicked(eventData.item)
                        return@OnMenuItemClickListener true
                    }
                    R.id.edit -> {
                        listener.onMenuEditClicked(eventData.item)
                        return@OnMenuItemClickListener true
                    }
                }
                false
            })
            popup.show()
        }
    }

}