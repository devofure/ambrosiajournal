package com.devofure.ambrosiajournal.presenter.ui.bookmark

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import com.devofure.ambrosiajournal.data.AmbrosiaJournalRepository
import com.devofure.ambrosiajournal.domain.DataResult
import com.devofure.ambrosiajournal.domain.MenuItem
import com.devofure.ambrosiajournal.domain.Restaurant
import com.devofure.ambrosiajournal.domain.generateDistance
import com.devofure.ambrosiajournal.presenter.base.BaseViewModel
import com.devofure.ambrosiajournal.presenter.base.Event
import com.devofure.ambrosiajournal.presenter.base.EventProcess.Finish
import com.devofure.ambrosiajournal.presenter.base.EventProcess.InProcess
import com.devofure.ambrosiajournal.presenter.base.EventProcessKey.LoadBookmarkedMenuItemProcess
import com.devofure.ambrosiajournal.presenter.base.EventProcessKey.LoadBookmarkedPlacesProcess
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import javax.inject.Inject

@HiltViewModel
class BookmarkViewModel @Inject constructor(
    repository: AmbrosiaJournalRepository,
) : BaseViewModel() {

    private val locationLiveData = MutableLiveData<LatLng>()

    suspend fun sortByLocation(
        list: List<Restaurant>,
    ): List<Restaurant> {
        val latLng = locationLiveData.value
        return latLng?.let {
            return list.onEach { it.generateDistance(latLng) }
                .sortedBy { it.currentDistance }
        } ?: list
    }

    fun setLocation(location: Location) {
        locationLiveData.value = LatLng(location.latitude, location.longitude)
    }

    val bookmarkedPlaces: LiveData<DataResult<List<Restaurant>>> = repository
        .loadBookmarkRestaurants()
        .onStart {
            _eventProcess.value = Event(InProcess(LoadBookmarkedPlacesProcess))
        }
        .map {
            if (it is DataResult.Success && locationLiveData.value != null) {
                DataResult.Success(sortByLocation(it.data), it.isCache)
            } else {
                it
            }
        }
        .onEach {
            if (it is DataResult.Success || it is DataResult.Error) {
                _eventProcess.value = Event(Finish(LoadBookmarkedPlacesProcess))
            }
        }.asLiveData()

    val bookmarkedMenuItems: LiveData<DataResult<List<MenuItem>>> = repository
        .loadBookmarkedMenuItems()
        .onStart {
            _eventProcess.value = Event(InProcess(LoadBookmarkedMenuItemProcess))
        }
        .onEach {
            if (it is DataResult.Success || it is DataResult.Error) {
                _eventProcess.value = Event(Finish(LoadBookmarkedMenuItemProcess))
            }
        }
        .asLiveData()
}