package com.devofure.ambrosiajournal.presenter.adapter

import com.devofure.ambrosiajournal.R
import com.devofure.ambrosiajournal.databinding.ItemPhotoBinding
import com.devofure.ambrosiajournal.domain.Photo

class PhotoItemListAdapter :
    ItemListAdapter<Photo, ItemPhotoBinding>(R.layout.item_photo) {

    override fun onBindItemCreated(itemViewHolder: ItemViewHolder) = Unit

    override fun onBindItem(
        itemBinding: ItemPhotoBinding,
        eventData: BindingEventData<Photo>,
    ) {
        itemBinding.viewModelItem = eventData.item
        //itemBinding.setVariable(BR.navigationViewModel, navigationViewModel)
    }

}