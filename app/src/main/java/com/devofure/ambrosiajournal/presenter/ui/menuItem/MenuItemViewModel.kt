package com.devofure.ambrosiajournal.presenter.ui.menuItem

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.devofure.ambrosiajournal.data.AmbrosiaJournalRepository
import com.devofure.ambrosiajournal.domain.DataResult
import com.devofure.ambrosiajournal.domain.MenuItem
import com.devofure.ambrosiajournal.domain.Photo
import com.devofure.ambrosiajournal.domain.UserReviewNote
import com.devofure.ambrosiajournal.presenter.adapter.DomainModel
import com.devofure.ambrosiajournal.presenter.base.BaseViewModel
import com.devofure.ambrosiajournal.presenter.base.Event
import com.devofure.ambrosiajournal.presenter.base.EventProcess.Finish
import com.devofure.ambrosiajournal.presenter.base.EventProcess.InProcess
import com.devofure.ambrosiajournal.presenter.base.EventProcessKey.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.lang.System.currentTimeMillis
import java.util.*
import javax.inject.Inject

inline class MenuItemKey(val id: String)
inline class UserReviewNoteId(val id: String)

@FlowPreview
@ExperimentalCoroutinesApi
@HiltViewModel
class MenuItemViewModel
@Inject constructor(
    private val repository: AmbrosiaJournalRepository,
) : BaseViewModel() {

    var saveReviewNoteByUser: Boolean = false
        private set

    var saveMenuItemActionByUser: Boolean = false
        private set

    var rateMenuItemActionByUser: Boolean = false
        private set

    @ExperimentalCoroutinesApi
    private val _menuItemChannel = ConflatedBroadcastChannel<MenuItemKey>()

    @ExperimentalCoroutinesApi
    private val _reviewNoteChannel = ConflatedBroadcastChannel<UserReviewNoteId>()

    val rateSelected = MutableLiveData(1.0f)

    val menuItem: LiveData<DataResult<MenuItem>> = _menuItemChannel
        .asFlow()
        .map { result ->
            _eventProcess.value = Event(InProcess(SearchPlacesProcess))
            result
        }
        .flatMapLatest { menuItemId -> repository.retrieveMenuItem(menuItemId.id) }
        //.catch { _eventProcess.value = Error(READ_RESTAURANT, it) }
        .map { result ->
            _eventProcess.value = Event(Finish(SearchPlacesProcess))
            result
        }
        .asLiveData()
    val userReviewNote: LiveData<DataResult<UserReviewNote>> = _reviewNoteChannel
        .asFlow()
        .map { result ->
            _eventProcess.value = Event(InProcess(SearchPlacesProcess))
            result
        }
        .flatMapLatest { reviewNoteId -> repository.retrieveUserReviewNote(reviewNoteId.id) }
        //.catch { _eventProcess.value = Error(READ_RESTAURANT, it) }
        .map { result ->
            _eventProcess.value = Event(Finish(SearchPlacesProcess))
            result
        }
        .asLiveData()

    val menuItemPhotos: LiveData<DataResult<List<Photo>>> = _menuItemChannel
        .asFlow()
        .onStart { _eventProcess.value = Event(InProcess(LoadMenuItemProcess)) }
        .flatMapLatest { restaurantKey -> repository.retrieveMenuItemPhotos(restaurantKey.id) }
        .catch {
            notifyCommonExceptions(it, LoadMenuItemProcess)
        }
        .onCompletion { _eventProcess.value = Event(Finish(LoadMenuItemProcess)) }
        .asLiveData()

    val userReviewNoteList: LiveData<DataResult<List<UserReviewNote>>> = _menuItemChannel
        .asFlow()
        .onStart { _eventProcess.value = Event(InProcess(LoadReviewNoteProcess)) }
        .flatMapLatest { menuItemKey -> repository.retrieveUserReviewNoteList(menuItemKey.id) }
        //.catch { _eventProcess.value = Error(READ_RESTAURANT, it) }
        .onCompletion { _eventProcess.value = Event(Finish(LoadReviewNoteProcess)) }
        .asLiveData()

    fun <I : DomainModel> switchBookmark(item: I) {
        when (item) {
            is MenuItem -> viewModelScope.launch {
                if (item.id != null) {
                    item.favorite = item.favorite != true
                    repository.updateMenuItemFavorite(item.id!!, item.favorite!!)
                }
            }
        }
    }

    fun setMenuItemId(menuItemId: String) {
        viewModelScope.launch {
            _menuItemChannel.send(MenuItemKey(menuItemId))
        }
    }

    fun setReviewNoteId(reviewNoteId: String) {
        viewModelScope.launch {
            _reviewNoteChannel.send(UserReviewNoteId(reviewNoteId))
        }
    }

    fun rateMenuItem(rate: Int, actionByUser: Boolean) {
        val dataResult = menuItem.value
        if (dataResult is DataResult.Success) {
            this.rateMenuItemActionByUser = actionByUser
            val menuItem = dataResult.data
            if (menuItem.id == null) return
            viewModelScope.launch {
                runEventProcess(
                    RateMenuItemProcess,
                    actionEvent = { repository.updateMenuItemRate(menuItem.id, rate) },
                    handleExceptionEvent = { t -> notifyCommonExceptions(t, RateMenuItemProcess) }
                )
            }
        }
    }

    fun upsertMenuItemName(menuItem: MenuItem, actionByUser: Boolean) {
        this.saveMenuItemActionByUser = actionByUser
        viewModelScope.launch {
            runEventProcess(
                SaveMenuItemProcess,
                actionEvent = {
                    if (menuItem.id != null) {
                        repository.updateMenuItemName(menuItem.id, menuItem.name)
                    } else {
                        repository.saveNewMenuItem(menuItem.name, menuItem.restaurantId)
                    }
                },
                handleExceptionEvent = { t -> notifyCommonExceptions(t, SaveMenuItemProcess) }
            )
        }
    }

    fun saveMenuItemReviewNote(reviewNote: UserReviewNote, actionByUser: Boolean) {
        this.saveReviewNoteByUser = actionByUser
        viewModelScope.launch {
            runEventProcess(
                SaveReviewNoteProcess,
                actionEvent = { repository.saveOrReplace(reviewNote) },
                handleExceptionEvent = { t -> notifyCommonExceptions(t, SaveReviewNoteProcess) }
            )
        }
    }

    fun delete() {
        val dataResult = menuItem.value
        if (dataResult is DataResult.Success) {
            viewModelScope.launch {
                repository.deleteMenuItem(dataResult.data)
            }
        }
    }

    fun createTempMenuItem(restaurantId: String) = MenuItem(restaurantId = restaurantId, name = "")

    fun createTempReviewNote(menuItemId: String) =
        UserReviewNote(
            id = UUID.randomUUID().toString(),
            menuItemId = menuItemId,
            lastChanged = currentTimeMillis()
        )

    fun deleteReviewNote(userReviewNote: UserReviewNote) {
        viewModelScope.launch {
            repository.deleteReviewNote(userReviewNote)
        }
    }

    fun addImage(
        menuItemId: String,
        absolutePath: String,
        name: String,
        description: String?,
        maxHeight: Long,
        maxWidth: Long,
    ) {
        viewModelScope.launch {
            repository.addImageToMenuItem(menuItemId,
                description,
                absolutePath,
                name,
                maxHeight,
                maxWidth)
            repository.insertOrIgnorePhoto(menuItemId, absolutePath)
        }
    }
}